# Tasks

-   [ ] Create Nitride.Core that doesn't depend on Serilog and Autofac?

## CLI

-   [ ] `watch` command to generate pages
-   [ ] Fancy Specter-based display

## Testing

Write tests for the following namespaces:

-   [ ] Calendar
-   [ ] Feeds
-   [ ] Gemtext
-   [ ] Handlebars
-   [ ] Html
-   [x] IO
-   [ ] Markdown
-   [ ] Slugs
-   [ ] Temporal
-   [ ] Yaml

## Examples

-   [ ] Write more examples
