# Nitride CIL

A static site generator written in C#.

## Entity Component System

It is build on [Gallium ECS](https://gitlab.com/mfgames-cil/gallium-cil/), an [entity-component-system](https://en.wikipedia.org/wiki/Entity_component_system) that allows for a flexibility of adding and removing aspects of individual files including creating new ones from JSON/YAML, web servers, or the file system. Additional markers and details can be added without significant changes, allowing for new concepts that weren't envisioned by the original developers.

## Configuration as Code

Because it can be difficult to explain the complexities of a website with just a collection of templates and scripts, this library uses C# for the configuration. In effect, you create a program that generates a website from source files.

## Unix Philosophy

This library is designed with the [Unix Philosophy](https://en.wikipedia.org/wiki/Unix_philosophy):

> Do one thing well.

It generates website HTML from a variety of sources (mostly Markdown). The core library doesn't try to wrangle Javascript or minify CSS. It doesn't have plugins for pushing up to S3 or copy files. Those are things [Webpack](https://webpack.js.org/), [Parcel](https://parceljs.org/), `scp`, or other tools specialize in.
