# Nitride.IO

This assembly contains the primary system for reading and writing from the disk,
along with various processes to manipulate paths. It contains three primary
components:

-   File System I/O
-   Path Normalization
-   Disk-Based Content

## File System I/O

Internally, this assembly uses [Zio](https://github.com/xoofx/zio), an file
system abstraction that also has an in-memory implementation which is ideal for
running unit tests. Zio also provides a way of treating an arbitrary directory
as a root directory so all paths are relative to the "root".

## Path Normalization

Zio also provides `UPath`, a normalization class that handles relative and
absolute paths. Entities that are read from the disk, such as with `ReadFiles`,
will have a `UPath` component with the path from the file. This component is
also used to determine the path when writing out the results.

## Disk-Based Content

This assembly also extends the `Nitride.Contents.IBinaryContent` and
`Nitride.Contents.ITextContent` to have file system based implementations. These
keep track of the original path and file system regardless of changes made to
the `UPath` component.
