using System.IO;
using System.Text;

using Nitride.Contents;

using Zio;

namespace Nitride.IO.Contents;

/// <summary>
/// Contains a wrapper around a file entry to retrieve text data.
/// </summary>
public class FileEntryTextContent : ITextContent, IBinaryContentConvertable
{
    private readonly FileEntry entry;

    public FileEntryTextContent(FileEntry entry)
    {
        this.entry = entry;
    }

    /// <inheritdoc />
    public TextReader GetReader()
    {
        return new StreamReader(this.entry.Open(FileMode.Open, FileAccess.Read, FileShare.Read), Encoding.UTF8);
    }

    /// <inheritdoc />
    public IBinaryContent ToBinaryContent()
    {
        return new FileEntryBinaryContent(this.entry);
    }
}
