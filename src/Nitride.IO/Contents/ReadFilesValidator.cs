using FluentValidation;

namespace Nitride.IO.Contents;

public class ReadFilesValidator : AbstractValidator<ReadFiles>
{
    public ReadFilesValidator()
    {
        this.RuleFor(x => x.Pattern)
            .NotNull();

        this.RuleFor(x => x.FileSystem)
            .NotNull();
    }
}
