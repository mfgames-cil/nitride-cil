using System.IO;

using Nitride.Contents;

using Zio;

namespace Nitride.IO.Contents;

/// <summary>
/// Contains a wrapper around a file entry to retrieve the binary data.
/// </summary>
public class FileEntryBinaryContent : IBinaryContent, ITextContentConvertable
{
    private readonly FileEntry entry;

    public FileEntryBinaryContent(FileEntry entry)
    {
        this.entry = entry;
    }

    /// <inheritdoc />
    public Stream GetStream()
    {
        return this.entry.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
    }

    /// <inheritdoc />
    public ITextContent ToTextContent()
    {
        return new FileEntryTextContent(this.entry);
    }
}
