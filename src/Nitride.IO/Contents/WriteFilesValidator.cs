using FluentValidation;

namespace Nitride.IO.Contents;

public class WriteFilesValidator : AbstractValidator<WriteFiles>
{
    public WriteFilesValidator()
    {
        this.RuleFor(x => x.StreamFactories)
            .NotNull();

        this.RuleFor(x => x.TextEncoding)
            .NotNull();

        this.RuleFor(x => x.FileSystem)
            .NotNull();

        this.RuleFor(x => x.Logger)
            .NotNull();
    }
}
