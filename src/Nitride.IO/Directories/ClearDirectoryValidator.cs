using FluentValidation;

namespace Nitride.IO.Directories;

public class ClearDirectoryValidator : AbstractValidator<ClearDirectory>
{
    public ClearDirectoryValidator()
    {
        this.RuleFor(x => x.Path)
            .NotNull();

        this.RuleFor(x => x.FileSystem)
            .NotNull();

        this.RuleFor(x => x.Logger)
            .NotNull();
    }
}
