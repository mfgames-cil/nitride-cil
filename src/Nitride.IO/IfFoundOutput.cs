namespace Nitride.IO;

public enum IfFoundOutput
{
    /// <summary>
    /// If the entity is found, then remove it from output.
    /// </summary>
    RemoveFromOutput,

    /// <summary>
    /// If the entity is found, then keep it in the output sequence.
    /// </summary>
    ReturnInOutput,
}
