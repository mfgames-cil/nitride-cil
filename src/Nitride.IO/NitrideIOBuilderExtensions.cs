using Autofac;

namespace Nitride.IO;

public static class NitrideIOBuilderExtensions
{
    public static NitrideBuilder UseIO(this NitrideBuilder builder)
    {
        return builder.ConfigureContainer(x => x.RegisterModule<NitrideIOModule>());
    }
}
