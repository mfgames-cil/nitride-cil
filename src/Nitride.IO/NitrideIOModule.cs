using Autofac;

namespace Nitride.IO;

public class NitrideIOModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
        builder.RegisterValidators(this);
    }
}
