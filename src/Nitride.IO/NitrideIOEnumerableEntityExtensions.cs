using System.Collections.Generic;

using Gallium;

using MAB.DotIgnore;

using Zio;

namespace Nitride.IO;

/// <summary>
/// Extension methods for working with paths.
/// </summary>
public static class NitrideIOEnumerableEntityExtensions
{
    /// <summary>
    /// Retrieves an entity from the sequence of entities.
    /// </summary>
    /// <param name="input">The sequence of iterations to parse.</param>
    /// <param name="path">The path to search for.</param>
    /// <param name="foundEntity">The entity pulled out, if found. Otherwise null.</param>
    /// <param name="removeFromResults">If true, then remove the entity from the list.</param>
    /// <returns>The sequence of entities, optionally without one.</returns>
    public static IEnumerable<Entity> GetEntityByPath(
        this IEnumerable<Entity> input,
        UPath path,
        out Entity? foundEntity,
        IfFoundOutput removeFromResults = IfFoundOutput.RemoveFromOutput)
    {
        List<Entity> output = new();
        foundEntity = null;

        foreach (Entity entity in input)
        {
            // If we don't have a path, it isn't it.
            if (!entity.TryGet(out UPath entityPath))
            {
                output.Add(entity);

                continue;
            }

            // See if the path matches. If it doesn't, then return it.
            if (entityPath != path)
            {
                output.Add(entity);

                continue;
            }

            // We found the entity, so optionally return it.
            foundEntity = entity;

            if (removeFromResults == IfFoundOutput.ReturnInOutput)
            {
                output.Add(entity);
            }
        }

        // Return the resulting output.
        return output;
    }

    /// <summary>
    /// Filters out entities that match a .gitignore style list and returns
    /// the remaining entities.
    /// </summary>
    public static IEnumerable<Entity> WhereNotIgnored(
        this IEnumerable<Entity> input,
        IgnoreList ignoreList)
    {
        foreach (Entity entity in input)
        {
            // If we don't have a path, nothing to do.
            if (!entity.TryGet(out UPath path))
            {
                yield return entity;
            }

            // See if the path matches. We use the "path is directory" set to false
            // because Entity represents files, not directories.
            string text = path.ToString();

            if (!ignoreList.IsIgnored(text, false))
            {
                yield return entity;
            }
        }
    }
}
