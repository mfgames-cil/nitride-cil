using System.Collections.Generic;

using Gallium;

using Zio;

namespace Nitride.IO;

public abstract class FileSystemOperationBase : IOperation
{
    protected FileSystemOperationBase(IFileSystem fileSystem)
    {
        this.FileSystem = fileSystem;
    }

    public IFileSystem FileSystem { get; set; }

    /// <inheritdoc />
    public abstract IEnumerable<Entity> Run(IEnumerable<Entity> input);
}
