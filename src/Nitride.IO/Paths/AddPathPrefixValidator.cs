using FluentValidation;

namespace Nitride.IO.Paths;

public class AddPathPrefixValidator : AbstractValidator<AddPathPrefix>
{
    public AddPathPrefixValidator()
    {
        this.RuleFor(x => x.PathPrefix)
            .NotNull();
    }
}
