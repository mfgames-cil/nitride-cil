using System;
using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Zio;

namespace Nitride.IO.Paths;

/// <summary>
/// A pipeline operation that replaces the UPath of the given entity
/// with the results of a lambda output. Entities without a path component
/// are passed on without touching.
/// </summary>
[WithProperties]
public partial class ReplacePath : IOperation
{
    private readonly IValidator<ReplacePath> validator;

    public ReplacePath(IValidator<ReplacePath> validator)
    {
        this.validator = validator;
    }

    /// <summary>
    /// Gets or sets the replacement callback to alter the paths.
    /// </summary>
    public Func<Entity, UPath, UPath> Replacement { get; set; } = null!;

    /// <summary>
    /// Performs the replacement on the input streams and outputs the
    /// resulting entities. Only entities that have had their paths changed
    /// will be updated, the others will be passed on as-is.
    /// </summary>
    /// <param name="input">The list of input entities.</param>
    /// <returns>The output entities.</returns>
    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return input.SelectEntity<UPath>(
            (
                entity,
                oldPath) =>
            {
                UPath newPath = this.Replacement(entity, oldPath);

                return newPath != oldPath ? entity.Set(newPath) : entity;
            });
    }
}
