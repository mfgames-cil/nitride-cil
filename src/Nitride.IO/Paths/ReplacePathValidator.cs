using FluentValidation;

namespace Nitride.IO.Paths;

public class ReplacePathValidator : AbstractValidator<ReplacePath>
{
    public ReplacePathValidator()
    {
        this.RuleFor(x => x.Replacement)
            .NotNull();
    }
}
