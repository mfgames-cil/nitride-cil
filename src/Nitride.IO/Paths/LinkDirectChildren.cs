using System.Collections.Generic;
using System.Linq;

using FluentValidation;

using Gallium;

using Nitride.Entities;

using Serilog;

using Zio;

namespace Nitride.IO.Paths;

[WithProperties]
public partial class LinkDirectChildren : CreateOrUpdateIndex
{
    /// <inheritdoc />
    public LinkDirectChildren(
        ILogger logger,
        IValidator<CreateOrUpdateIndex> validator)
        : base(logger, validator)
    {
        this.UpdateIndex = this.InternalUpdateIndex;

        this.GetIndexKey = e => e.Get<UPath>()
            .GetDirectoryIndexPath();
    }

    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        if (this.Scanner != null!)
        {
            return base.Run(input);
        }

        this.Scanner = new DirectChildPathScanner(new EntityScannerValidator());

        input = this.Scanner.Run(input)
            .ToList();

        return base.Run(input);
    }

    private Entity InternalUpdateIndex(
        Entity entity,
        string _,
        IEnumerable<Entity> list)
    {
        return entity.Add(new DirectChildEntityList(list));
    }
}
