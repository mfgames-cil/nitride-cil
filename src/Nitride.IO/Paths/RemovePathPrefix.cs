using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Zio;

namespace Nitride.IO.Paths;

/// <summary>
/// An operation that removes a path prefix from the input.
/// </summary>
[WithProperties]
public partial class RemovePathPrefix : IOperation
{
    private readonly ReplacePath replacePath;

    private readonly IValidator<RemovePathPrefix> validator;

    public RemovePathPrefix(
        IValidator<RemovePathPrefix> validator,
        ReplacePath replacePath)
    {
        this.validator = validator;
        this.replacePath = replacePath;
    }

    /// <summary>
    /// Gets or sets the prefix for the path operations.
    /// </summary>
    public UPath PathPrefix { get; set; }

    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return this.replacePath.WithReplacement(this.RunReplacement)
            .Run(input);
    }

    private UPath RunReplacement(
        Entity _,
        UPath path)
    {
        string normalized = path.ToString();
        string prefix = this.PathPrefix.ToString()!;

        if (normalized.StartsWith(prefix))
        {
            return (UPath)path.ToString()
                .Substring(prefix.Length);
        }

        return path;
    }
}
