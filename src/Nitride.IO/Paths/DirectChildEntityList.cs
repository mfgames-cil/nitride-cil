using System.Collections.Generic;

using Gallium;

namespace Nitride.IO.Paths;

/// <summary>
/// A wrapper around List&lt;Entity&gt; for handling direct children lists.
/// </summary>
public class DirectChildEntityList : List<Entity>
{
    /// <inheritdoc />
    public DirectChildEntityList()
    {
    }

    /// <inheritdoc />
    public DirectChildEntityList(IEnumerable<Entity> collection)
        : base(collection)
    {
    }
}
