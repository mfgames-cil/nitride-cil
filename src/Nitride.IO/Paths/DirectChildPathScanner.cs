using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Nitride.Entities;

using Zio;

namespace Nitride.IO.Paths;

/// <summary>
/// Implements a scanner that gathers up all the direct child files
/// and folders underneath a given path.
/// </summary>
[WithProperties]
public partial class DirectChildPathScanner : EntityScanner
{
    /// <inheritdoc />
    public DirectChildPathScanner(IValidator<EntityScanner> validator)
        : base(validator)
    {
        this.GetKeysFromEntity = this.InternalGetKeysFromEntity;
    }

    private IEnumerable<string>? InternalGetKeysFromEntity(Entity entity)
    {
        // Get the path for the entity. If we don't have a path, then there
        // is nothing to do.
        if (!entity.TryGet(out UPath path))
        {
            return null;
        }

        // If we are a root path, then we don't have a parent.
        if (path.IsEmpty || path.IsNull || path == "/")
        {
            return null;
        }

        // If we are using directory indexes, skip when we have an index root.
        // Otherwise, get the parent and use that as the key.
        return path.GetDirectoryIndexPath() == "/" ? null : new[] { path.GetParentDirectoryIndexPath() };
    }
}
