using Zio;

namespace Nitride.IO.Paths;

/// <summary>
/// Extension methods for the UPath class.
/// </summary>
public static class UPathExtensions
{
    /// <summary>
    /// Gets the directory path, which excludes the directory at the end
    /// of the path.
    /// </summary>
    /// <param name="path">The path to manipulate.</param>
    /// <returns>A normalized path.</returns>
    public static string GetDirectoryIndexPath(this UPath path)
    {
        if (path.GetNameWithoutExtension() == "index")
        {
            return path.GetDirectory()
                    .ToString()
                    .TrimEnd('/')
                + "/";
        }

        return path.ToString();
    }

    public static string GetParentDirectoryIndexPath(this UPath path)
    {
        UPath indexPath = path.GetDirectoryIndexPath();
        UPath parent = indexPath.GetDirectory();

        if (parent == null!)
        {
            parent = "/";
        }

        string parentPath = parent.ToString()
                .TrimEnd('/')
            + "/";

        return parentPath;
    }
}
