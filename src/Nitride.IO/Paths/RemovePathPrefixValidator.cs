using FluentValidation;

namespace Nitride.IO.Paths;

public class RemovePathPrefixValidator : AbstractValidator<RemovePathPrefix>
{
    public RemovePathPrefixValidator()
    {
        this.RuleFor(x => x.PathPrefix)
            .NotNull();
    }
}
