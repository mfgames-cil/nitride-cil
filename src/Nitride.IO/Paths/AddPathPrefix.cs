using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Zio;

namespace Nitride.IO.Paths;

[WithProperties]
public partial class AddPathPrefix : OperationBase
{
    private readonly ReplacePath replacePath;

    private readonly IValidator<AddPathPrefix> validator;

    public AddPathPrefix(
        IValidator<AddPathPrefix> validator,
        ReplacePath replacePath)
    {
        this.validator = validator;
        this.replacePath = replacePath;
    }

    /// <summary>
    /// Gets or sets the prefix for the path operations.
    /// </summary>
    public UPath? PathPrefix { get; set; }

    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return this.replacePath.WithReplacement(this.RunReplacement)
            .Run(input);
    }

    private UPath RunReplacement(
        Entity _,
        UPath path)
    {
        string innerRelativePath = path.ToString()
            .TrimStart('/');

        return this.PathPrefix!.Value / innerRelativePath;
    }
}
