using FluentValidation;

namespace Nitride.IO.Paths;

public class MoveToIndexPathValidator : AbstractValidator<MoveToIndexPath>
{
    public MoveToIndexPathValidator()
    {
        this.RuleFor(x => x.CanMoveCallback)
            .NotNull();
    }
}
