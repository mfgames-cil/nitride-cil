using FluentValidation;

namespace Nitride.IO.Paths;

public class ChangePathExtensionValidator : AbstractValidator<ChangePathExtension>
{
    public ChangePathExtensionValidator()
    {
        this.RuleFor(x => x.Extension)
            .NotNull();
    }
}
