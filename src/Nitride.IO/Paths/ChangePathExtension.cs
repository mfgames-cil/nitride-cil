using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Zio;

namespace Nitride.IO.Paths;

/// <summary>
/// Changes the extension of the paths given.
/// </summary>
[WithProperties]
public partial class ChangePathExtension : IOperation
{
    private readonly ReplacePath replacePath;

    private readonly IValidator<ChangePathExtension> validator;

    public ChangePathExtension(
        IValidator<ChangePathExtension> validator,
        ReplacePath replacePath)
    {
        this.validator = validator;
        this.replacePath = replacePath;
    }

    /// <summary>
    /// Gets or sets the prefix for the path operations.
    /// </summary>
    public string? Extension { get; set; }

    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return this.replacePath.WithReplacement(this.RunReplacement)
            .Run(input);
    }

    private UPath RunReplacement(
        Entity _,
        UPath path)
    {
        return path.ChangeExtension(this.Extension!);
    }
}
