namespace Nitride;

/// <summary>
/// Indicates an operation that resolved (completely processes the input)
/// before returning from the `Run` operation.
/// </summary>
public interface IResolvingOperation : IOperation
{
}
