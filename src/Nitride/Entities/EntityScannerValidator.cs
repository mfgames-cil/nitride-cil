using FluentValidation;

namespace Nitride.Entities;

public class EntityScannerValidator : AbstractValidator<EntityScanner>
{
    public EntityScannerValidator()
    {
        this.RuleFor(x => x.GetKeysFromEntity)
            .NotNull();
    }
}
