using FluentValidation;

namespace Nitride.Entities;

public class CreateOrUpdateIndexValidator : AbstractValidator<CreateOrUpdateIndex>
{
    public CreateOrUpdateIndexValidator()
    {
        this.RuleFor(x => x.Scanner)
            .NotNull();

        this.RuleFor(x => x.GetIndexKey)
            .NotNull();

        this.RuleFor(x => x.UpdateIndex)
            .NotNull();
    }
}
