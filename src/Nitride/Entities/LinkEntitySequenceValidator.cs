using FluentValidation;

namespace Nitride.Entities;

public class LinkEntitySequenceValidator : AbstractValidator<LinkEntitySequence>
{
    public LinkEntitySequenceValidator()
    {
        this.RuleFor(x => x.CreateSequenceIndex)
            .NotNull();

        this.RuleFor(x => x.AddSequenceIndex)
            .NotNull();
    }
}
