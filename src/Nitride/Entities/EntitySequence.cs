using System.Collections.Generic;

using Gallium;

namespace Nitride.Entities;

/// <summary>
/// Defines a relationship between a given entity and the others in a sequence.
/// </summary>
public class EntitySequence
{
    public EntitySequence(
        IReadOnlyList<Entity> sequence,
        int index)
    {
        this.Sequence = sequence;
        this.Index = index;
    }

    public bool HasNext => this.Sequence.Count > 0 && !this.IsLast;

    public bool HasPrevious => this.Sequence.Count > 0 && !this.IsFirst;

    public int Index { get; }

    public bool IsFirst => this.Sequence.Count > 0 && this.Index == 0;

    public bool IsLast => this.Sequence.Count > 0 && this.Index == this.Sequence.Count - 1;

    public Entity? Next => this.HasNext ? this.Sequence[this.Index + 1] : null;

    public Entity? Previous => this.HasPrevious ? this.Sequence[this.Index - 1] : null;

    public IReadOnlyList<Entity> Sequence { get; }
}
