using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading.Tasks;

using Nitride.Pipelines;

using Serilog;

namespace Nitride.Commands;

/// <summary>
/// The basic command to generate a website and run through the pipelines.
/// </summary>
public class BuildCommand : Command, ICommandHandler
{
    private readonly ILogger logger;

    private readonly IList<IPipelineCommandOption> pipelineOptions;

    private readonly PipelineManager pipelines;

    public BuildCommand(
        ILogger logger,
        PipelineManager pipelines,
        IList<IPipelineCommandOption> pipelineOptions)
        : base("build", "Generate the website")
    {
        // Set up our simple member variables.
        this.pipelines = pipelines;
        this.pipelineOptions = pipelineOptions;
        this.logger = logger.ForContext<BuildCommand>();
        this.Handler = this;

        // Handle any injected arguments into the command line.
        foreach (IPipelineCommandOption? option in pipelineOptions)
        {
            this.AddOption(option.Option);
        }
    }

    /// <inheritdoc />
    public async Task<int> InvokeAsync(InvocationContext context)
    {
        // Process any injected options.
        this.logger.Debug("Processing {Count:N0} pipeline options", this.pipelineOptions.Count);

        foreach (IPipelineCommandOption? option in this.pipelineOptions)
        {
            this.logger.Verbose("Processing pipeline option: {Option}", option);
            option.Handle(context);
        }

        // This is the main entry point into the `build` command. This runs
        // all the pipelines once and then quits when it finishes.
        this.logger.Information("Running pipelines");

        int pipelinesResults = await this.pipelines.RunAsync();

        return pipelinesResults;
    }
}
