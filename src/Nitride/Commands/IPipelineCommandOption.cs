using System.CommandLine;
using System.CommandLine.Invocation;

namespace Nitride.Commands;

/// <summary>
/// Creates new options for the build command to allow injecting an
/// option into the build command.
/// </summary>
public interface IPipelineCommandOption
{
    /// <summary>
    /// The option that used to bind to the command.
    /// </summary>
    Option Option { get; }

    /// <summary>
    /// Handles the command after it has been created and the options bound.
    /// </summary>
    /// <param name="context">The context of the CLI command.</param>
    void Handle(InvocationContext context);
}
