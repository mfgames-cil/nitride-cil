using System.Collections.Generic;

using Gallium;

namespace Nitride;

/// <summary>
/// Contains common functionality useful for Nitride operations.
/// </summary>
public abstract class OperationBase : IOperation
{
    /// <inheritdoc />
    public abstract IEnumerable<Entity> Run(IEnumerable<Entity> input);
}
