using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Nitride;

/// <summary>
/// Helper methods for getting the names of properties.
/// </summary>
public static class ExpressionHelper
{
    public static string GetMemberName<T>(Expression<Func<T, object?>> expression)
    {
        return GetMemberName(expression.Body);
    }

    public static IEnumerable<string> GetMemberNames<T>(params Expression<Func<T, object?>>[] expressions)
    {
        return expressions.Select(x => GetMemberName(x.Body));
    }

    public static IEnumerable<string> GetMemberNames<T>(
        this T instance,
        params Expression<Func<T, object?>>[] expressions)
    {
        return GetMemberNames<T>(expressions);
    }

    private static string GetMemberName(Expression expression)
    {
        if (expression == null)
        {
            throw new ArgumentException(nameof(expression));
        }

        if (expression is MemberExpression memberExpression)
        {
            return memberExpression.Member.Name;
        }

        if (expression is MethodCallExpression methodCallExpression)
        {
            return methodCallExpression.Method.Name;
        }

        if (expression is UnaryExpression unaryExpression)
        {
            return GetMemberName(unaryExpression);
        }

        throw new ArgumentException(nameof(expression));
    }

    private static string GetMemberName(UnaryExpression unaryExpression)
    {
        if (unaryExpression.Operand is MethodCallExpression methodCallExpression)
        {
            return methodCallExpression.Method.Name;
        }

        return ((MemberExpression)unaryExpression.Operand).Member.Name;
    }
}
