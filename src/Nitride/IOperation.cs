using System.Collections.Generic;

using Gallium;

namespace Nitride;

public interface IOperation
{
    /// <summary>
    /// Runs the input entities through the operation and returns the results.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    IEnumerable<Entity> Run(IEnumerable<Entity> input);
}
