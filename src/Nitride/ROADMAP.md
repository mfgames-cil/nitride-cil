# Project Roadmap

## Immediate

-   Switch the various operations to be async
    -   ReadFiles
    -   WriteFiles
-   Implement mime type determination
-   Implement a convert to text content based on mime type
