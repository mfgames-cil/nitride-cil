using System;
using System.IO;

namespace Nitride.Contents;

/// <summary>
/// Implements a purely in-memory binary content provider.
/// </summary>
public class ByteArrayBinaryContent : IBinaryContent
{
    private readonly byte[] buffer;

    public ByteArrayBinaryContent(byte[] buffer)
    {
        this.buffer = buffer ?? throw new ArgumentNullException(nameof(buffer));
    }

    /// <inheritdoc />
    public Stream GetStream()
    {
        // Return a memory stream that cannot be written.
        return new MemoryStream(this.buffer, false);
    }
}
