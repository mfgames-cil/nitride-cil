using System.IO;

namespace Nitride.Contents;

/// <summary>
/// Indicates a content source that works with binary data and streams.
/// </summary>
public interface IBinaryContent : IContent
{
    /// <summary>
    /// Opens a read-only stream into the underlying source of binary data.
    /// </summary>
    /// <remarks>
    /// It is up to the calling class to close this stream.
    /// </remarks>
    /// <returns>A stream to the internal data.</returns>
    Stream GetStream();
}
