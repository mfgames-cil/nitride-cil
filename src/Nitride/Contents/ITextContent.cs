using System.IO;

namespace Nitride.Contents;

/// <summary>
/// Indicates a content source that provides data as a TextReader and text.
/// </summary>
public interface ITextContent : IContent
{
    /// <summary>
    /// Opens a text reader into the underlying source of text data.
    /// </summary>
    /// <remarks>
    /// It is up to the calling class to close this reader.
    /// </remarks>
    /// <returns>The text reader to the internal data.</returns>
    TextReader GetReader();
}
