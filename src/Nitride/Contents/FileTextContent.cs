using System;
using System.IO;
using System.Text;

namespace Nitride.Contents;

/// <summary>
/// A text content provider based on a file in the file system.
/// </summary>
public class FileTextContent : ITextContent, IBinaryContentConvertable
{
    private readonly string path;

    public FileTextContent(string path)
    {
        this.path = path ?? throw new ArgumentNullException(nameof(path));
    }

    public FileTextContent(FileInfo file)
    {
        if (file == null)
        {
            throw new ArgumentNullException(nameof(file));
        }

        this.path = file.FullName;
    }

    /// <inheritdoc />
    public TextReader GetReader()
    {
        return new StreamReader(File.Open(this.path, FileMode.Open, FileAccess.Read, FileShare.Read), Encoding.UTF8);
    }

    /// <inheritdoc />
    public IBinaryContent ToBinaryContent()
    {
        return new FileBinaryContent(this.path);
    }
}
