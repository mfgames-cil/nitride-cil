namespace Nitride.Contents;

/// <summary>
/// Describes a content provider or source for an Entity. `IContent` is
/// also used for the mutual exclusivity login in `EntityContentExtensions`
/// where all `IContent` derived components are automatically removed when
/// setting a new content source.
/// </summary>
public interface IContent
{
}
