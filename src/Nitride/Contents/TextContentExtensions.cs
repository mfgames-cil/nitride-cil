using System.IO;

using Gallium;

namespace Nitride.Contents;

public static class TextContentExtensions
{
    public static string GetText(this ITextContent content)
    {
        var writer = new StringWriter();
        using TextReader reader = content.GetReader();
        string? line = reader.ReadLine();

        while (line != null)
        {
            writer.WriteLine(line);
            line = reader.ReadLine();
        }

        return writer.ToString();
    }

    public static string? GetText(this IBinaryContent content)
    {
        if (content is ITextContentConvertable convertableContent)
        {
            return convertableContent
                .ToTextContent()
                .GetText();
        }

        return null;
    }

    public static string? GetText(this Entity entity)
    {
        if (entity.TryGet(out ITextContent textContent))
        {
            return textContent.GetText();
        }

        if (entity.TryGet(out IBinaryContent binaryContent))
        {
            return binaryContent.GetText();
        }

        return null;
    }
}
