using Gallium;

namespace Nitride.Contents;

/// <summary>
/// Various extension methods for working with binary content.
/// </summary>
public static class EntityBinaryContentExtensions
{
    /// <summary>
    /// Retrieves the binary content associated with this entity.
    /// </summary>
    /// <param name="entity">The entity to query.</param>
    /// <returns>The binary content.</returns>
    public static IBinaryContent GetBinaryContent(this Entity entity)
    {
        return entity.Get<IBinaryContent>();
    }

    /// <summary>
    /// Determines if the entity has a registered IBinaryContent component.
    /// </summary>
    /// <param name="entity">The entity to inspect.</param>
    /// <returns>True if there is a component extending `IContent`.</returns>
    public static bool HasBinaryContent(this Entity entity)
    {
        return entity.Has<IBinaryContent>();
    }

    /// <summary>
    /// Remove all existing content components from the entity and then adds
    /// the new binary content as a component into the entity before
    /// returning it.
    /// </summary>
    /// <param name="entity">The entity to query and modify.</param>
    /// <param name="content">The content to add to the entity.</param>
    /// <typeparam name="TType">The base type of the content.</typeparam>
    /// <returns>The entity or a copy with the new component.</returns>
    public static Entity SetBinaryContent<TType>(
        this Entity entity,
        TType content)
        where TType : IBinaryContent
    {
        return entity.SetContent<IBinaryContent>(content);
    }
}
