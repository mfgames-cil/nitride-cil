using System;
using System.IO;
using System.Text;

namespace Nitride.Contents;

/// <summary>
/// Implements a pure, in-memory text content provider that uses a string
/// as the backing store.
/// </summary>
public class StringTextContent : ITextContent
{
    private readonly string buffer;

    public StringTextContent(StringBuilder buffer)
        : this(buffer.ToString())
    {
    }

    public StringTextContent(string buffer)
    {
        this.buffer = buffer ?? throw new ArgumentNullException(nameof(buffer));
    }

    public StringTextContent(params string[] lines)
        : this(string.Join("\n", lines))
    {
    }

    /// <inheritdoc />
    public TextReader GetReader()
    {
        return new StringReader(this.buffer);
    }
}
