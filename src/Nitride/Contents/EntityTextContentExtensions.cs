using Gallium;

namespace Nitride.Contents;

/// <summary>
/// Various extension methods for working with text content.
/// </summary>
public static class EntityTextContentExtensions
{
    /// <summary>
    /// Determines if the entity has a registered ITextContent component.
    /// </summary>
    /// <param name="entity">The entity to inspect.</param>
    /// <returns>True if there is a component extending `IContent`.</returns>
    public static bool HasTextContent(this Entity entity)
    {
        return entity.Has<ITextContent>();
    }

    /// <summary>
    /// Remove all existing content components from the entity and then adds
    /// the new text content as a component into the entity before
    /// returning it.
    /// </summary>
    /// <param name="entity">The entity to query and modify.</param>
    /// <param name="content">The content to add to the entity.</param>
    /// <typeparam name="TType">The base type of the content.</typeparam>
    /// <returns>The entity or a copy with the new component.</returns>
    public static Entity SetTextContent<TType>(
        this Entity entity,
        TType content)
        where TType : ITextContent
    {
        return entity.SetContent<ITextContent>(content);
    }

    /// <summary>
    /// Remove all existing content components from the entity and then adds
    /// the new text content as a component into the entity before
    /// returning it.
    /// </summary>
    /// <param name="entity">The entity to query and modify.</param>
    /// <param name="content">The content to add to the entity.</param>
    /// <typeparam name="TType">The base type of the content.</typeparam>
    /// <returns>The entity or a copy with the new component.</returns>
    public static Entity SetTextContent(
        this Entity entity,
        string content)
    {
        return entity.SetTextContent(new StringTextContent(content));
    }
}
