namespace Nitride.Contents;

/// <summary>
/// An interface to indicates an object can be converted into a text
/// content directly.
/// </summary>
public interface ITextContentConvertable
{
    /// <summary>
    /// Convert the given object into a text content.
    /// </summary>
    /// <returns></returns>
    ITextContent ToTextContent();
}
