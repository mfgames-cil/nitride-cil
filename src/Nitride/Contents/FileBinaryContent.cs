using System;
using System.IO;

namespace Nitride.Contents;

/// <summary>
/// A binary content provider based on a file in the file system.
/// </summary>
public class FileBinaryContent : IBinaryContent, ITextContentConvertable
{
    private readonly string path;

    public FileBinaryContent(string path)
    {
        this.path = path ?? throw new ArgumentNullException(nameof(path));
    }

    public FileBinaryContent(FileInfo file)
    {
        if (file == null)
        {
            throw new ArgumentNullException(nameof(file));
        }

        this.path = file.FullName;
    }

    /// <inheritdoc />
    public Stream GetStream()
    {
        return File.Open(this.path, FileMode.Open, FileAccess.Read, FileShare.Read);
    }

    /// <inheritdoc />
    public ITextContent ToTextContent()
    {
        return new FileTextContent(this.path);
    }
}
