namespace Nitride.Contents;

/// <summary>
/// An interface to indicates an object can be converted into a binary
/// content directly.
/// </summary>
public interface IBinaryContentConvertable
{
    /// <summary>
    /// Convert the given object into a binary content.
    /// </summary>
    /// <returns></returns>
    IBinaryContent ToBinaryContent();
}
