namespace Nitride;

/// <summary>
/// Describes the signature of a callback for after the container is build.
/// </summary>
public delegate void ContainerBuiltHandler();
