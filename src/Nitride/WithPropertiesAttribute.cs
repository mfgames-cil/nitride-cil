using System;

namespace Nitride;

/// <summary>
/// A marker attribute that indicates that the source generator should
/// automatically add `Set*` methods for every public property in the class.
/// The class must be `partial` for this to work.
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class WithPropertiesAttribute : Attribute
{
}
