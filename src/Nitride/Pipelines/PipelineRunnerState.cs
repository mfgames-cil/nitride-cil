namespace Nitride.Pipelines;

/// <summary>
/// Describes the state of the pipelines which is used both for reporting
/// purposes but also to allow the pipelines to respond to changes in their
/// own state, such as unloading output elements.
/// </summary>
public enum PipelineRunnerState
{
    /// <summary>
    /// Indicates that the runner is setting up and not ready for any use.
    /// </summary>
    Initializing,

    /// <summary>
    /// Indicates that the runner has been initialized.
    /// </summary>
    Initialized,

    /// <summary>
    /// Indicates that the pipeline is prepare for a new run. This is done
    /// when the system determines it needs to run.
    /// </summary>
    Preparing,

    /// <summary>
    /// Indicates that the pipeline has finished preparing.
    /// </summary>
    Prepared,

    /// <summary>
    /// Indicates that the runner is waiting for dependencies to run.
    /// </summary>
    Waiting,

    /// <summary>
    /// Indicates that the pipeline has started processing by a call to
    /// `RunAsync`.
    /// </summary>
    Started,

    /// <summary>
    /// Indicates that the pipeline is providing data to any dependencies.
    /// If the pipeline has no dependencies, then this will never be called.
    /// </summary>
    Providing,

    /// <summary>
    /// Indicates that all the dependencies on this pipeline has finished
    /// running (state `Finished`) and the pipeline can clean up any
    /// memory elements.
    /// </summary>
    Provided,

    /// <summary>
    /// Indicates that the pipeline is done running and the system is
    /// shutting down.
    /// </summary>
    Finalizing,

    /// <summary>
    /// Indicates that the pipeline has been completely cleaned up and
    /// finished. It will never be used again.
    /// </summary>
    Finalized,

    /// <summary>
    /// Indicates that there was an error while running the pipeline.
    /// </summary>
    Errored,
}
