using System.Collections.Generic;
using System.Threading.Tasks;

using Gallium;

namespace Nitride.Pipelines;

/// <summary>
/// A basic pipeline that is configured through properties and methods.
/// </summary>
public abstract class PipelineBase : IPipeline
{
    private readonly List<IPipeline> dependencies;

    protected PipelineBase()
    {
        this.dependencies = new List<IPipeline>();
    }

    public PipelineBase AddDependency(IPipeline pipeline)
    {
        this.dependencies.Add(pipeline);

        return this;
    }

    /// <inheritdoc />
    public IEnumerable<IPipeline> GetDependencies()
    {
        return this.dependencies;
    }

    /// <inheritdoc />
    public abstract IAsyncEnumerable<Entity> RunAsync(IEnumerable<Entity> entities);

    /// <inheritdoc />
    public override string ToString()
    {
        return this.GetType()
            .Name;
    }
}
