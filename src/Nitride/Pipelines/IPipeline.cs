using System.Collections.Generic;
using System.Threading.Tasks;

using Gallium;

namespace Nitride.Pipelines;

/// <summary>
/// Implements the basic signature for a pipeline, a distinct unit of
/// processing that reads, manipulates, and writes data.
/// </summary>
public interface IPipeline
{
    /// <summary>
    /// Gets the dependencies for the pipeline.
    /// </summary>
    IEnumerable<IPipeline> GetDependencies();

    /// <summary>
    /// Performs various operations such as reading, writing, and
    /// transformation on the given entities before returning a new
    /// collection, which may or may not include some of the original
    /// entities.
    /// </summary>
    /// <param name="entities">The entities to process.</param>
    /// <returns>The resulting entities after the process runs.</returns>
    IAsyncEnumerable<Entity> RunAsync(IEnumerable<Entity> entities);
}
