using Autofac;

using FluentValidation;

namespace Nitride;

public static class NitrideModuleExtensions
{
    public static void RegisterOperators(
        this ContainerBuilder builder,
        Module module)
    {
        builder.RegisterAssemblyTypes(
                module.GetType()
                    .Assembly)
            .Where(x => x.IsAssignableTo<IOperation>())
            .AsSelf();
    }

    public static void RegisterValidators(
        this ContainerBuilder builder,
        Module module)
    {
        builder.RegisterAssemblyTypes(
                module.GetType()
                    .Assembly)
            .AsClosedTypesOf(typeof(IValidator<>));
    }
}
