using System.Collections.Generic;

using Gallium;

namespace Nitride;

/// <summary>
/// Extension methods to run a Nitride operation inline with code.
/// </summary>
public static class NitrideOperationExtensions
{
    /// <summary>
    /// Runs the given configured operation against the input and returns
    /// the results.
    /// </summary>
    /// <param name="input">The entities to perform the operation against.</param>
    /// <param name="operation">The operation to run.</param>
    /// <returns>The results of the operation.</returns>
    public static IEnumerable<Entity> Run(
        this IEnumerable<Entity> input,
        IOperation operation)
    {
        return operation.Run(input);
    }
}
