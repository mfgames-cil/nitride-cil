using System.Collections.Generic;
using System.CommandLine;

using Autofac;

using Nitride.Commands;
using Nitride.Pipelines;

namespace Nitride;

public class NitrideModule : Module
{
    public string? ApplicationName { get; set; }

    /// <summary>
    /// Gets or sets the description of the website.
    /// </summary>
    public string? Description { get; set; }

    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        // Pipelines
        builder.RegisterType<PipelineRunner>()
            .AsSelf();

        builder.RegisterType<PipelineManager>()
            .AsSelf()
            .SingleInstance();

        // Operations
        builder.RegisterValidators(this);
        builder.RegisterOperators(this);

        // Commands
        builder.RegisterType<BuildCommand>()
            .AsSelf()
            .As<Command>()
            .SingleInstance();

        // MfGames.ToolBuilder requires the RootCommand to be registered. This is because
        // of various things, mostly coordinating between different systems.
        builder.Register(
                c =>
                {
                    // Create the new root command.
                    var root = new RootCommand();

                    if (!string.IsNullOrWhiteSpace(this.ApplicationName))
                    {
                        root.Name = this.ApplicationName;
                    }

                    if (!string.IsNullOrWhiteSpace(this.Description))
                    {
                        root.Description = this.Description;
                    }

                    ;

                    // Add in the commands.
                    IEnumerable<Command> commands = c.Resolve<IEnumerable<Command>>();

                    foreach (Command command in commands)
                    {
                        root.AddCommand(command);
                    }

                    return root;
                })
            .AsSelf();
    }
}
