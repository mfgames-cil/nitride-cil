namespace Nitride.Markdown;

/// <summary>
/// A marker class that indicates that the file is a Markdown file.
/// </summary>
public record IsMarkdown
{
    public static IsMarkdown Instance { get; } = new();
}
