using System;

using FluentValidation;

using Gallium;

using Markdig;

using MfGames.Markdown.Gemtext;

using Nitride.Contents;
using Nitride.Gemtext;

namespace Nitride.Markdown;

/// <summary>
/// Converts the input Markdown files into Gemtext using Markdig and
/// MfGames.Markdown.Gemtext. This only processes files with a text input
/// and the IsMarkdown component.
/// </summary>
public class ConvertMarkdownToGemtext : ConvertMarkdownToBase
{
    public ConvertMarkdownToGemtext(IValidator<ConvertMarkdownToBase> validator)
        : base(validator)
    {
    }

    /// <inheritdoc />
    public override ConvertMarkdownToGemtext WithConfigureMarkdown(Action<MarkdownPipelineBuilder>? value)
    {
        base.WithConfigureMarkdown(value);

        return this;
    }

    /// <summary>
    /// Converts the Markdown file into HTML.
    /// </summary>
    /// <param name="entity">The entity to convert.</param>
    /// <param name="markdownContent">The content for this entity.</param>
    /// <param name="options">The markdown pipeline.</param>
    /// <returns>A converted entity.</returns>
    protected override Entity Convert(
        Entity entity,
        ITextContent markdownContent,
        MarkdownPipeline options)
    {
        string markdown = markdownContent.GetText();
        string gemtext = MarkdownGemtext.ToGemtext(markdown, options);
        var content = new StringTextContent(gemtext);

        entity = entity.SetTextContent(content)
            .Remove<IsMarkdown>()
            .Set(IsGemtext.Instance);

        return entity;
    }
}
