using FluentValidation;

namespace Nitride.Markdown;

public class ConvertMarkdownToBaseValidator : AbstractValidator<ConvertMarkdownToBase>
{
    public ConvertMarkdownToBaseValidator()
    {
        this.RuleFor(x => x.ConfigureMarkdown)
            .NotNull();
    }
}
