using System;
using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Markdig;

using Nitride.Contents;

namespace Nitride.Markdown;

[WithProperties]
public abstract partial class ConvertMarkdownToBase : IOperation
{
    private readonly IValidator<ConvertMarkdownToBase> validator;

    protected ConvertMarkdownToBase(IValidator<ConvertMarkdownToBase> validator)
    {
        this.validator = validator;
    }

    /// <summary>
    /// Gets or sets an additional callback to configure additional features
    /// from the baseline Markdown.
    /// </summary>
    public Action<MarkdownPipelineBuilder>? ConfigureMarkdown { get; set; }

    /// <inheritdoc />
    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        // Validate the inputs.
        this.validator.ValidateAndThrow(this);

        // Create the Markdown pipeline used for formatting.
        var builder = new MarkdownPipelineBuilder();

        this.ConfigureMarkdown?.Invoke(builder);

        MarkdownPipeline options = builder.Build();

        // Process the Markdown files (while passing everything on).
        return input.SelectEntity<IsMarkdown, ITextContent>(
            (
                entity,
                _,
                content) => this.Convert(entity, content, options));
    }

    /// <summary>
    /// Converts the Markdown file into HTML.
    /// </summary>
    /// <param name="entity">The entity to convert.</param>
    /// <param name="markdownContent">The content for this entity.</param>
    /// <param name="options">The markdown pipeline.</param>
    /// <returns>A converted entity.</returns>
    protected abstract Entity Convert(
        Entity entity,
        ITextContent markdownContent,
        MarkdownPipeline options);
}
