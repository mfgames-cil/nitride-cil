using FluentValidation;

namespace Nitride.Markdown;

public class IdentifyMarkdownValidator : AbstractValidator<IdentifyMarkdown>
{
    public IdentifyMarkdownValidator()
    {
        this.RuleFor(x => x.IsMarkdownTest)
            .NotNull();
    }
}
