using FluentValidation;

using Gallium;

using Zio;

namespace Nitride.Markdown;

public class IdentifyMarkdownFromPath : IdentifyMarkdown
{
    public IdentifyMarkdownFromPath(IValidator<IdentifyMarkdown> validator)
        : base(validator)
    {
        this.IsMarkdownTest = DefaultIsMarkdown;
    }

    private static bool DefaultIsMarkdown(
        Entity entity,
        UPath path)
    {
        return (path.GetExtensionWithDot() ?? string.Empty).ToLowerInvariant() switch
        {
            ".md" => true,
            ".markdown" => true,
            _ => false,
        };
    }
}
