using System;
using System.Collections.Generic;

using FluentValidation;

using Gallium;

using Nitride.Contents;

using Zio;

namespace Nitride.Markdown;

/// <summary>
/// An operation that identifies Markdown files by their common extensions
/// and converts them to text input while also adding the IsMarkdown
/// component to identify them.
/// </summary>
[WithProperties]
public partial class IdentifyMarkdown : IOperation
{
    private readonly IValidator<IdentifyMarkdown> validator;

    public IdentifyMarkdown(IValidator<IdentifyMarkdown> validator)
    {
        this.validator = validator;
    }

    public Func<Entity, UPath, bool> IsMarkdownTest { get; set; } = null!;

    /// <inheritdoc />
    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return input.SelectEntity<UPath, ITextContent>(this.MarkTextEntities)
            .SelectEntity<UPath, IBinaryContent>(this.MarkBinaryEntities);
    }

    private Entity MarkBinaryEntities(
        Entity entity,
        UPath path,
        IBinaryContent binary)
    {
        // If we aren't a Markdown file, then there is nothing we can do about that.
        if (!this.IsMarkdownTest(entity, path))
        {
            return entity;
        }

        // Convert the file as a binary.
        if (binary is ITextContentConvertable textConvertable)
        {
            entity = entity.SetTextContent(textConvertable.ToTextContent())
                .Set(IsMarkdown.Instance);
        }
        else
        {
            throw new InvalidOperationException(
                "Cannot convert a binary content to a text without ITextContentConvertable.");
        }

        return entity;
    }

    private Entity MarkTextEntities(
        Entity entity,
        UPath path,
        ITextContent _)
    {
        // If we aren't a Markdown file, then there is nothing
        // we can do about that.
        if (!this.IsMarkdownTest(entity, path))
        {
            return entity;
        }

        // We are already text, so just mark it as Markdown.
        entity = entity.Set(IsMarkdown.Instance);

        return entity;
    }
}
