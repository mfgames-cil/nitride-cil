using Autofac;

namespace Nitride.Markdown;

public static class NitrideMarkdownBuilderExtensions
{
    public static NitrideBuilder UseMarkdown(this NitrideBuilder builder)
    {
        return builder.ConfigureContainer(x => x.RegisterModule<NitrideMarkdownModule>());
    }
}
