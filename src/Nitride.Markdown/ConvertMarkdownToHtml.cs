using System;

using FluentValidation;

using Gallium;

using Markdig;

using Nitride.Contents;
using Nitride.Html;

namespace Nitride.Markdown;

/// <summary>
/// Converts the input Markdown files into HTML using Markdig. This only
/// processes files with a text input and the IsMarkdown component.
/// </summary>
public class ConvertMarkdownToHtml : ConvertMarkdownToBase
{
    public ConvertMarkdownToHtml(IValidator<ConvertMarkdownToBase> validator)
        : base(validator)
    {
    }

    /// <inheritdoc />
    public override ConvertMarkdownToHtml WithConfigureMarkdown(Action<MarkdownPipelineBuilder>? value)
    {
        base.WithConfigureMarkdown(value);

        return this;
    }

    /// <summary>
    /// Converts the Markdown file into HTML.
    /// </summary>
    /// <param name="entity">The entity to convert.</param>
    /// <param name="markdownContent">The content for this entity.</param>
    /// <param name="options">The markdown pipeline.</param>
    /// <returns>A converted entity.</returns>
    protected override Entity Convert(
        Entity entity,
        ITextContent markdownContent,
        MarkdownPipeline options)
    {
        // Convert the entity to Html.
        string markdown = markdownContent.GetText();
        string html = Markdig.Markdown.ToHtml(markdown, options);
        var htmlContent = new StringTextContent(html);

        entity = entity.SetTextContent(htmlContent)
            .Remove<IsMarkdown>()
            .Set(IsHtml.Instance);

        // Return the resulting entity.
        return entity;
    }
}
