using Autofac;

namespace Nitride.Markdown;

public class NitrideMarkdownModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
        builder.RegisterValidators(this);
    }
}
