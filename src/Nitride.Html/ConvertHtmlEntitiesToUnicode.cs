using System.Collections.Generic;
using System.Net;

using Gallium;

using Nitride.Contents;

namespace Nitride.Html;

/// <summary>
/// Converts the text input that uses HTML entities and turns them into
/// Unicode variations.
/// </summary>
public class ConvertHtmlEntitiesToUnicode : OperationBase
{
    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        return input.SelectEntity<ITextContent>(this.ResolveHtmlEntities);
    }

    private Entity ResolveHtmlEntities(
        Entity entity,
        ITextContent content)
    {
        string text = content.GetText();
        string resolved = WebUtility.HtmlDecode(text);

        return entity.SetTextContent(resolved);
    }
}
