using Autofac;

namespace Nitride.Html;

public class NitrideHtmlModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
    }
}
