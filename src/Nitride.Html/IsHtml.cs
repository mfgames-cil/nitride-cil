namespace Nitride.Html;

/// <summary>
/// A marker component that indicates that the entity is an HTML file.
/// </summary>
public record IsHtml
{
    public static IsHtml Instance { get; } = new();
}
