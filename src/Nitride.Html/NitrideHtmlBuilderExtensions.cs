using Autofac;

namespace Nitride.Html;

public static class NitrideHtmlBuilderExtensions
{
    public static NitrideBuilder UseHtml(this NitrideBuilder builder)
    {
        return builder.ConfigureContainer(x => x.RegisterModule<NitrideHtmlModule>());
    }
}
