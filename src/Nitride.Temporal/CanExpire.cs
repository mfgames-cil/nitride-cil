namespace Nitride.Temporal;

/// <summary>
/// A marker component for identifying a post that can expire.
/// </summary>
public class CanExpire
{
    public static CanExpire Instance { get; } = new();
}
