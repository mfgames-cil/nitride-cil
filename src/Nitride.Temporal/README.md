# Date Processing

One of the common features of static websites are blogs which leads to having
some form of date-centric processing of pages to build archive pages, calendars,
and being able to write posts in the future.

With the component system, the date of a given file is simply attached to
`Nodatime.Instant` component of the `Entity` object for the bulk of the
processing.

## Supporting Time Zones

The concept of time zones while date processing is one that is frequently
overlooked. A date is a date, right? However, most blogs and news sites have a
concept of when a new day starts but it isn't always the same time as the server
that is building the site. While a blog might be in America/Chicago time, a CI
server could be set to UTC (such as Azure build servers) and the
"day" may roll over fix or six hours before or after the blog's time.

This is why Nitride uses `Instant` for when pages are implemented. These are
points in time that are independent of time zones, but we also provide tools for
converting a date model or one from the path into a proper instant based on the
blog's time zone.

## Why NodaTime?

We decided to use [NodaTime](https://nodatime.org/) instead of the built-in date
time functions for a number of reasons, mainly because it has a more intuitive
way of handling time zones

## Configuring

There are two callbacks on `NitrideBuilder` that can be used to define the date
and time processing for the blog.

```csharp
NitrideBuilder builder;

builder
    .ConfigureDates((NitrideClock clock) => clock.SetTimeZone())
```
