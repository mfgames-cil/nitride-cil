using System.CommandLine;
using System.CommandLine.Invocation;
using System.Globalization;

using Nitride.Commands;

using NodaTime;

using Serilog;

using TimeSpanParserUtil;

namespace Nitride.Temporal.Cli;

/// <summary>
/// A factory to inject the "--expires=XXXX" argument into the build
/// and other pipeline commands.
/// </summary>
public class ExpiresPipelineCommandOption : IPipelineCommandOption
{
    private readonly Timekeeper clock;

    private readonly ILogger logger;

    public ExpiresPipelineCommandOption(
        ILogger logger,
        Timekeeper clock,
        string? defaultValue = null)
    {
        this.logger = logger.ForContext<Instant>();
        this.clock = clock;

        this.Option = new Option<string?>("--expires", () => defaultValue)
        {
            Description = "Sets the expiration time as time before the current date",
            ArgumentHelpName = "TIMESPAN",
        };
    }

    /// <inheritdoc />
    public Option Option { get; }

    /// <inheritdoc />
    public void Handle(InvocationContext context)
    {
        // If we have a format, then we are going to set one. If the format
        // is blank or never, then we are going to treat it as not
        // expiring anything.
        string? value = (string?)context.ParseResult.GetValueForOption(this.Option);

        if (value != null && value.ToLowerInvariant() != "never")
        {
            // Parse the format using TimeSpanParser.
            this.clock.Expires = TimeSpanParser.Parse(value);
        }

        // If we don't have anything, then just report and we're done.
        if (this.clock.Expiration == null)
        {
            this.logger.Information("No entities will be expired");

            return;
        }

        // Figure out when we are going to be expiring.
        Instant when = this.clock.Expiration.Value;
        ZonedDateTime dateTime = when.InZone(this.clock.DateTimeZone);
        string formatted = dateTime.ToString("yyyy-MM-dd HH:mm:ss x", CultureInfo.CurrentCulture);

        this.logger.Information("Expiring entries before {When:l}", formatted);
    }
}
