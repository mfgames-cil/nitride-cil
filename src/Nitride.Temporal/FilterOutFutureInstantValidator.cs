using FluentValidation;

namespace Nitride.Temporal;

public class FilterOutFutureInstantValidator : AbstractValidator<FilterOutFutureInstant>
{
    public FilterOutFutureInstantValidator()
    {
        this.RuleFor(x => x.Timekeeper)
            .NotNull();
    }
}
