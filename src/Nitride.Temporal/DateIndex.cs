using System.Collections.Generic;

using Gallium;

namespace Nitride.Temporal;

public class DateIndex
{
    public DateIndex(
        string key,
        string format,
        IReadOnlyList<Entity> entries,
        IReadOnlyList<Entity> indexes)
    {
        this.Key = key;
        this.Format = format;
        this.Entries = entries;
        this.Indexes = indexes;
    }

    /// <summary>
    /// Gets the list of entries that are in this index.
    /// </summary>
    public IReadOnlyList<Entity> Entries { get; }

    /// <summary>
    /// Gets the format used to create this index.
    /// </summary>
    public string Format { get; }

    /// <summary>
    /// Gets the ordered list of nested indexes, if there are any. This will be an
    /// empty list if the index has no
    /// sub-index.
    /// </summary>
    public IReadOnlyList<Entity> Indexes { get; }

    /// <summary>
    /// Gets the key for the index, which is a formatted date.
    /// </summary>
    public string Key { get; }
}
