using System.Collections.Generic;

using FluentValidation;

using Gallium;

using NodaTime;

namespace Nitride.Temporal;

/// <summary>
/// Filters out all expired entities that are before the expiration.
/// </summary>
[WithProperties]
public partial class FilterOutExpiredInstant : OperationBase
{
    private readonly IValidator<FilterOutExpiredInstant> validator;

    public FilterOutExpiredInstant(
        IValidator<FilterOutExpiredInstant> validator,
        Timekeeper clock)
    {
        this.validator = validator;
        this.Timekeeper = clock;
    }

    public Timekeeper Timekeeper { get; set; }

    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        if (!this.Timekeeper.Expiration.HasValue)
        {
            return input;
        }

        return input.SelectEntity<Instant, CanExpire>(this.IsNotExpired);
    }

    private Entity? IsNotExpired(
        Entity entity,
        Instant instant,
        CanExpire _)
    {
        Instant expiration = this.Timekeeper.Expiration!.Value;
        bool isExpired = instant.CompareTo(expiration) < 0;

        return isExpired ? null : entity;
    }
}
