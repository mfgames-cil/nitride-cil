using System;

using NodaTime;

namespace Nitride.Temporal;

/// <summary>
/// Configures the temporal settings for use with `UseTemporal`.
/// </summary>
[WithProperties]
public partial class NitrideTemporalConfiguration
{
    /// <summary>
    /// Adds the "--date=XXXX-XX-XX" option into the pipeline commands.
    /// </summary>
    public bool AddDateOptionToCommandLine { get; set; }

    public bool AddExpireOptionToCommandLine { get; set; }

    /// <summary>
    /// Gets or sets the time zone to use for date time operations. Examples would be
    /// "America/Chicago".
    /// </summary>
    public DateTimeZone? DateTimeZone { get; set; }

    public string? Expiration { get; set; }

    public NitrideTemporalConfiguration WithDateOptionCommandLineOption()
    {
        return this.WithAddDateOptionToCommandLine(true);
    }

    public NitrideTemporalConfiguration WithDateTimeZone(string zoneName)
    {
        this.DateTimeZone = DateTimeZoneProviders.Tzdb[zoneName];

        return this;
    }

    /// <summary>
    /// Adds the "--expire" option into the pipeline commands where with the value set
    /// to
    /// the current timestamp (or the one provided by --date) plus the given timespan
    /// such
    /// as "500000:00:00.0".
    /// </summary>
    public NitrideTemporalConfiguration WithExpiresCommandLineOption(TimeSpan? timeSpan)
    {
        return this.WithExpiresCommandLineOption(timeSpan?.ToString());
    }

    /// <summary>
    /// Adds the "--expire=XXXX" option into the pipeline commands where
    /// "XXX" is a format like "5y" or "500000:00:00.0". This is parsed by
    /// TimeSpanParser which gives an easy format.
    /// </summary>
    public NitrideTemporalConfiguration WithExpiresCommandLineOption(string? defaultValue)
    {
        this.AddExpireOptionToCommandLine = defaultValue != null;
        this.Expiration = defaultValue;

        return this;
    }
}
