using FluentValidation;

namespace Nitride.Temporal;

public class SetInstantFromComponentValidator<TComponent> : AbstractValidator<SetInstantFromComponent<TComponent>>
{
    public SetInstantFromComponentValidator()
    {
        this.RuleFor(x => x.GetDateTimeObject)
            .NotNull();
    }
}
