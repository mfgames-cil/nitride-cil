using System.Collections.Generic;

using Gallium;

using NodaTime;

namespace Nitride.Temporal;

/// <summary>
/// Filters out all entities that have an instant before the current
/// NitrideClock time.
/// </summary>
[WithProperties]
public partial class FilterOutFutureInstant : OperationBase
{
    public FilterOutFutureInstant(Timekeeper timekeeper)
    {
        this.Timekeeper = timekeeper;
    }

    public Timekeeper Timekeeper { get; set; }

    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        Instant now = this.Timekeeper.Clock.GetCurrentInstant();

        return input
            .SelectEntity<Instant>(
                (
                        entity,
                        instant) =>
                    instant.CompareTo(now) <= 0 ? entity : null);
    }
}
