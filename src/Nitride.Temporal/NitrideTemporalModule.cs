using Autofac;

namespace Nitride.Temporal;

public class NitrideTemporalModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
        builder.RegisterValidators(this);

        builder.RegisterType<Timekeeper>()
            .AsSelf()
            .SingleInstance();

        builder.RegisterGeneric(typeof(SetInstantFromComponent<>))
            .As(typeof(SetInstantFromComponent<>));
    }
}
