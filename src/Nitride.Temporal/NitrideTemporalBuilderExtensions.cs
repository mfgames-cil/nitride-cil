using System;

using Autofac;

using Nitride.Commands;
using Nitride.Temporal.Cli;

using Serilog;

using Expires = Nitride.Temporal.Cli.ExpiresPipelineCommandOption;

namespace Nitride.Temporal;

public static class NitrideTemporalBuilderExtensions
{
    /// <summary>
    /// Extends the builder to allow for configuring the temporal
    /// settings for generation.
    /// </summary>
    public static NitrideBuilder UseTemporal(
        this NitrideBuilder builder,
        Action<NitrideTemporalConfiguration>? configure = null)
    {
        // Get the configuration so we can set the various options.
        var config = new NitrideTemporalConfiguration();

        configure?.Invoke(config);

        // Add in the module registration.
        builder.ConfigureContainer(
            x =>
            {
                // Register the module.
                x.RegisterModule<NitrideTemporalModule>();

                // Add in the CLI options.
                if (config.AddDateOptionToCommandLine)
                {
                    x.RegisterType<DatePipelineCommandOption>()
                        .As<IPipelineCommandOption>();
                }

                if (config.AddExpireOptionToCommandLine && config.Expiration != null)
                {
                    x.Register(
                            context =>
                            {
                                ILogger logger = context.Resolve<ILogger>();
                                Timekeeper clock = context.Resolve<Timekeeper>();

                                return new Expires(logger, clock, config.Expiration);
                            })
                        .As<IPipelineCommandOption>();
                }
            });

        if (config.DateTimeZone != null)
        {
            builder.ConfigureSite(
                (
                    _,
                    scope) =>
                {
                    ILogger logger = scope.Resolve<ILogger>();
                    Timekeeper timekeeper = scope.Resolve<Timekeeper>();

                    timekeeper.DateTimeZone = config.DateTimeZone;
                    logger.Verbose("Setting time zone to {Zone:l}", timekeeper.DateTimeZone);
                });
        }

        return builder;
    }
}
