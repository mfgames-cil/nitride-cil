using FluentValidation;

namespace Nitride.Temporal;

public class FilterOutExpiredInstantValidator : AbstractValidator<FilterOutExpiredInstant>
{
    public FilterOutExpiredInstantValidator()
    {
        this.RuleFor(x => x.Timekeeper)
            .NotNull();
    }
}
