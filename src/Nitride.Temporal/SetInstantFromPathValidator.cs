using FluentValidation;

namespace Nitride.Temporal;

public class SetInstantFromPathValidator : AbstractValidator<SetInstantFromPath>
{
    public SetInstantFromPathValidator()
    {
        this.RuleFor(x => x.PathRegex)
            .NotNull();
    }
}
