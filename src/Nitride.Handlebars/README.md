# Nitride.Handlebars

This is a collection
of [Handlebars](https://github.com/Handlebars-Net/Handlebars.Net)
operations that work with Nitride. There are two ways of using Handelbars in
this package.

## IHandlebars

This library does _not_ configure or register `IHandlebars` but requires it to
be injected into the DI container (Autofac) along with any templates or inlines
loaded.

## Styling Templates

The first is to use it to apply a Handlebars theme to a page or text content.
This would include adding links to the CSS, any javascript, generating menus,
and common navigation elements.

## Content Templates

The second is used to apply it to the Handlebars inside the text content. This
is the page-specific content that needs to be resolved. A good example of this
might be creating a "last five posts" page or embedding some metadata from the
YAML header into the page.
