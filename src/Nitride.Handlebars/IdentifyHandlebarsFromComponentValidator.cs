using FluentValidation;

namespace Nitride.Handlebars;

public class IdentifyHandlebarsFromComponentValidator : AbstractValidator<IdentifyHandlebarsFromComponent>
{
    public IdentifyHandlebarsFromComponentValidator()
    {
        this.RuleFor(x => x.HasHandlebarsTest)
            .NotNull();
    }
}
