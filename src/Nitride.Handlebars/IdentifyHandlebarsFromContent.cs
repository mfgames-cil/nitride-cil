using System.Collections.Generic;

using Gallium;

using Nitride.Contents;

namespace Nitride.Handlebars;

/// <summary>
/// An operation that discovers which text files have a Handlebars template
/// inside them.
/// </summary>
public class IdentifyHandlebarsFromContent : IOperation
{
    /// <inheritdoc />
    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        return input.SelectEntity<ITextContent>(this.ScanContent);
    }

    private Entity ScanContent(
        Entity entity,
        ITextContent content)
    {
        string text = content.GetText();

        if (text.Contains("{{") && text.Contains("}}"))
        {
            return entity.Set(HasHandlebarsTemplate.Instance);
        }

        return entity;
    }
}
