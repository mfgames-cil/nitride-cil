using System;
using System.Collections.Generic;

using FluentValidation;

using Gallium;

using HandlebarsDotNet;

using Nitride.Contents;

namespace Nitride.Handlebars;

/// <summary>
/// An operation that uses the content to create a template that is then
/// applied against the content and metadata and then replaces the content
/// of that entity.
/// </summary>
[WithProperties]
public partial class RenderContentTemplate : OperationBase
{
    private readonly HandlebarsTemplateCache cache;

    private readonly IValidator<RenderContentTemplate> validator;

    public RenderContentTemplate(
        IValidator<RenderContentTemplate> validator,
        HandlebarsTemplateCache cache)
    {
        this.validator = validator;
        this.cache = cache;
    }

    /// <summary>
    /// Gets or sets the callback used to create a model from a given
    /// entity. This allows for the website to customize what information is
    /// being passed to the template.
    /// </summary>
    public Func<Entity, object>? CreateModelCallback { get; set; }

    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return input.SelectEntity<HasHandlebarsTemplate, ITextContent>(this.Apply);
    }

    private Entity Apply(
        Entity entity,
        HasHandlebarsTemplate _,
        ITextContent content)
    {
        string text = content.GetText();
        HandlebarsTemplate<object, object> template = this.cache.GetLiteralTemplate(text);
        object model = this.CreateModelCallback!(entity);
        string result = template(model!);

        return entity
            .Remove<HasHandlebarsTemplate>()
            .SetTextContent(result);
    }
}
