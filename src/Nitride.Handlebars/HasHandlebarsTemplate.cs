namespace Nitride.Handlebars;

/// <summary>
/// A marker component that indicates that a given file with text component
/// has a Handlebars template in it.
/// </summary>
public record HasHandlebarsTemplate
{
    public static HasHandlebarsTemplate Instance { get; } = new();
}
