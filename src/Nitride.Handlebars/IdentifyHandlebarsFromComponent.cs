using System;
using System.Collections.Generic;
using System.Linq;

using FluentValidation;

using Gallium;

namespace Nitride.Handlebars;

/// <summary>
/// An operation that discovers which text files have a Handlebars template
/// based on a component.
/// </summary>
[WithProperties]
public partial class IdentifyHandlebarsFromComponent : IOperation
{
    private readonly IValidator<IdentifyHandlebarsFromComponent> validator;

    public IdentifyHandlebarsFromComponent(IValidator<IdentifyHandlebarsFromComponent> validator)
    {
        this.validator = validator;
    }

    public Func<Entity, bool> HasHandlebarsTest { get; set; } = null!;

    /// <inheritdoc />
    public IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return input.Select(
            (entity) => this.HasHandlebarsTest.Invoke(entity) ? entity.Set(HasHandlebarsTemplate.Instance) : entity);
    }
}
