using System;
using System.Collections.Generic;

using FluentValidation;

using Gallium;

using HandlebarsDotNet;

using Nitride.Contents;

namespace Nitride.Handlebars;

/// <summary>
/// An operation that applies a common or shared template on the content of
/// a document that includes theme or styling information.
/// </summary>
[WithProperties]
public partial class ApplyStyleTemplate : OperationBase
{
    private readonly HandlebarsTemplateCache cache;

    private readonly IValidator<ApplyStyleTemplate> validator;

    public ApplyStyleTemplate(
        IValidator<ApplyStyleTemplate> validator,
        HandlebarsTemplateCache cache)
    {
        this.validator = validator;
        this.cache = cache;
    }

    /// <summary>
    /// Gets or sets the callback used to create a model from a given
    /// entity. This allows for the website to customize what information is
    /// being passed to the template.
    /// </summary>
    public Func<Entity, object>? CreateModelCallback { get; set; }

    /// <summary>
    /// Gets or sets the callback used to determine which template to use
    /// for a given entity. This lets one have a per-page template change.
    /// </summary>
    public Func<Entity, string>? GetTemplateName { get; set; }

    public IHandlebars? Handlebars { get; set; }

    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        // Make sure we have sane data.
        this.validator.ValidateAndThrow(this);

        // Create and set up the Handlebars.
        return input.SelectEntity<ITextContent>(this.Apply);
    }

    private Entity Apply(
        Entity entity,
        ITextContent content)
    {
        object model = this.CreateModelCallback!(entity);
        string name = this.GetTemplateName!(entity);
        HandlebarsTemplate<object, object> template = this.cache.GetNamedTemplate(name);
        string result = template(model!);

        return entity.SetTextContent(result);
    }
}
