using FluentValidation;

namespace Nitride.Handlebars;

public class RenderContentTemplateValidator : AbstractValidator<RenderContentTemplate>
{
    public RenderContentTemplateValidator()
    {
        this.RuleFor(x => x.CreateModelCallback)
            .NotNull();
    }
}
