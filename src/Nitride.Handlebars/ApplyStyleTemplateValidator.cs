using FluentValidation;

namespace Nitride.Handlebars;

public class ApplyStyleTemplateValidator : AbstractValidator<ApplyStyleTemplate>
{
    public ApplyStyleTemplateValidator()
    {
        this.RuleFor(x => x.Handlebars)
            .NotNull();

        this.RuleFor(x => x.GetTemplateName)
            .NotNull();

        this.RuleFor(x => x.CreateModelCallback)
            .NotNull();
    }
}
