using System;
using System.Collections.Generic;

using Autofac;

using Nitride.Handlebars.Configuration;

namespace Nitride.Handlebars;

public static class NitrideHandlebarsBuilderExtensions
{
    public static NitrideBuilder UseHandlebars(this NitrideBuilder builder)
    {
        return builder
            .ConfigureContainer(x => x.RegisterModule<NitrideHandlebarsModule>());
    }

    public static NitrideBuilder UseHandlebars(
        this NitrideBuilder builder,
        Func<ContainerBuilder, IEnumerable<IHandlebarsLoader>> configure)
    {
        builder.UseHandlebars();

        builder.ConfigureContainer(
            c =>
            {
                IEnumerable<IHandlebarsLoader> loaders = configure(c);

                foreach (IHandlebarsLoader loader in loaders)
                {
                    c.RegisterInstance(loader)
                        .As<IHandlebarsLoader>()
                        .SingleInstance();
                }
            });

        return builder;
    }
}
