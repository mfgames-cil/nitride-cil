using System.IO;

using HandlebarsDotNet;

namespace Nitride.Handlebars.Configuration;

/// <summary>
/// Loads the templates from the given directory.
/// </summary>
public class FileSystemHandlebarsTemplateLoader : IHandlebarsLoader
{
    private readonly DirectoryInfo directory;

    private readonly string pattern;

    public FileSystemHandlebarsTemplateLoader(
        DirectoryInfo directory,
        string pattern = "*.hbs")
    {
        this.directory = directory;
        this.pattern = pattern;
    }

    /// <inheritdoc />
    public void Register(IHandlebars handlebars)
    {
        foreach (FileInfo file in this.directory.GetFiles(this.pattern))
        {
            string name = Path.GetFileNameWithoutExtension(file.Name);
            string content = File.ReadAllText(file.FullName);

            handlebars.RegisterTemplate(name, content);
        }
    }
}
