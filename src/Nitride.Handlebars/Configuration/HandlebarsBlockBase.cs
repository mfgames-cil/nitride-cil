using HandlebarsDotNet;

namespace Nitride.Handlebars.Configuration;

/// <summary>
/// Describes a block helper which can be registered.
/// </summary>
public abstract class HandlebarsBlockBase : IHandlebarsLoader
{
    /// <summary>
    /// Gets the name of the helper, which is how it is called in the template.
    /// </summary>
    protected abstract string HelperName { get; }

    public void Register(IHandlebars handlebars)
    {
        HandlebarsBlockHelper blockHelper = this.Render;

        handlebars.RegisterHelper(this.HelperName, blockHelper);
    }

    /// <summary>
    /// Renders the helper to the template.
    /// </summary>
    protected abstract void Render(
        EncodedTextWriter output,
        BlockHelperOptions options,
        Context context,
        Arguments input);
}
