using HandlebarsDotNet;

namespace Nitride.Handlebars.Configuration;

/// <summary>
/// Describes a dependency injected loader of templates or modules.
/// </summary>
public interface IHandlebarsLoader
{
    /// <summary>
    /// Registers the given helper into the handlebars engine.
    /// </summary>
    /// <param name="handlebars">The handlebars to register the helper into.</param>
    void Register(IHandlebars handlebars);
}
