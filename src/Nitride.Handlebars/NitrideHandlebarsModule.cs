using System.Collections.Generic;

using Autofac;

using HandlebarsDotNet;

using Nitride.Handlebars.Configuration;

namespace Nitride.Handlebars;

public class NitrideHandlebarsModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
        builder.RegisterValidators(this);

        builder.RegisterType<HandlebarsTemplateCache>()
            .AsSelf()
            .SingleInstance();

        builder.Register(
                (context) =>
                {
                    IHandlebars handlebars = HandlebarsDotNet.Handlebars.Create();
                    IEnumerable<IHandlebarsLoader> helpers = context.Resolve<IEnumerable<IHandlebarsLoader>>();

                    foreach (IHandlebarsLoader helper in helpers)
                    {
                        helper.Register(handlebars);
                    }

                    return handlebars;
                })
            .As<IHandlebars>()
            .SingleInstance();
    }
}
