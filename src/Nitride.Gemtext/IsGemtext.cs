namespace Nitride.Gemtext;

/// <summary>
/// A marker component for indicating that an entity is Gemtext, the format
/// for text files using the Gemini protocol.
/// </summary>
public record IsGemtext
{
    public static IsGemtext Instance { get; } = new();
}
