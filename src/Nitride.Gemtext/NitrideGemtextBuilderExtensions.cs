using Autofac;

namespace Nitride.Gemtext;

public static class NitrideGemtextBuilderExtensions
{
    public static NitrideBuilder UseGemtext(this NitrideBuilder builder)
    {
        return builder.ConfigureContainer(x => x.RegisterModule<NitrideGemtextModule>());
    }
}
