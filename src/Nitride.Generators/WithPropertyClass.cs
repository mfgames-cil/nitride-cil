using System.Collections.Generic;

using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Nitride.Generators;

/// <summary>
/// Internal class that consolidates all of the information needed to generate a
/// file.
/// </summary>
internal class WithPropertyClass
{
    /// <summary>
    /// Gets the syntax for the class declaration.
    /// </summary>
    public ClassDeclarationSyntax ClassDeclaration { get; set; } = null!;

    /// <summary>
    /// Gets or sets the namespace associated with the class.
    /// </summary>
    public string Namespace { get; set; } = null!;

    /// <summary>
    /// Gets the using statements that are in the class.
    /// </summary>
    public List<UsingDirectiveSyntax> UsingDirectiveList { get; set; } = new();
}
