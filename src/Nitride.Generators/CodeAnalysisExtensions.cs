using Microsoft.CodeAnalysis;

namespace Nitride.Generators;

/// <summary>
/// Various wrappers around the diagnostics to simplify generation.
/// </summary>
public static class CodeAnalysisExtensions
{
    /// <summary>
    /// Creates an error message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    public static void Error(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        string format,
        params object[] parameters)
    {
        Error(context, messageCode, null, format, parameters);
    }

    /// <summary>
    /// Creates an error message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="location">The optional location for the message.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    public static void Error(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        Location? location,
        string format,
        params object[] parameters)
    {
        context.Message(messageCode, location, DiagnosticSeverity.Error, format, parameters);
    }

    /// <summary>
    /// Creates an informational message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    public static void Information(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        string format,
        params object[] parameters)
    {
        Information(context, messageCode, null, format, parameters);
    }

    /// <summary>
    /// Creates an informational message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="location">The optional location for the message.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    public static void Information(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        Location? location,
        string format,
        params object[] parameters)
    {
        context.Message(messageCode, location, DiagnosticSeverity.Info, format, parameters);
    }

    /// <summary>
    /// Creates a warning message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    public static void Warning(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        string format,
        params object[] parameters)
    {
        Warning(context, messageCode, null, format, parameters);
    }

    /// <summary>
    /// Creates a warning message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="location">The optional location for the message.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    public static void Warning(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        Location? location,
        string format,
        params object[] parameters)
    {
        context.Message(messageCode, location, DiagnosticSeverity.Warning, format, parameters);
    }

    /// <summary>
    /// Creates a message to break the build while generating code.
    /// </summary>
    /// <param name="context">The context that contains the diagnostic.</param>
    /// <param name="messageCode">The normalized message code.</param>
    /// <param name="location">The optional location for the message.</param>
    /// <param name="format">The string format for the message.</param>
    /// <param name="parameters">The optional parameters.</param>
    /// <param name="severity">The severity of the message.</param>
    private static void Message(
        this GeneratorExecutionContext context,
        MessageCode messageCode,
        Location? location,
        DiagnosticSeverity severity,
        string format,
        params object[] parameters)
    {
        context.ReportDiagnostic(
            Diagnostic.Create(
                "GN" + ((int)messageCode).ToString("D4"),
                "Nitride",
                string.Format(format, parameters),
                severity,
                severity,
                true,
                severity is DiagnosticSeverity.Warning or DiagnosticSeverity.Info ? 4 : 0,
                location: location));
    }
}
