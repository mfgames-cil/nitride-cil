namespace Nitride.Generators;

/// <summary>
/// All the error messages produced by the generators.
/// </summary>
public enum MessageCode
{
    Debug = 1,
}
