using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Nitride.Slugs;

/// <summary>
/// Extends the slug provider to strip out accented characters for
/// a normalized form.
/// </summary>
public class UnicodeNormalizingSlugConverter : SimpleSlugConverter
{
    /// <inheritdoc />
    public UnicodeNormalizingSlugConverter()
    {
    }

    /// <inheritdoc />
    public UnicodeNormalizingSlugConverter(IDictionary<string, string> replacements)
        : base(replacements)
    {
    }

    /// <inheritdoc />
    public override string ToSlug(string input)
    {
        // If we have null or whitespace, we have a problem.
        if (string.IsNullOrWhiteSpace(input))
        {
            throw new ArgumentException("Cannot have a blank or null input", nameof(input));
        }

        // Normalize the Unicode objects.
        // Strip out the accents. This is a cheesy way of doing so.
        char[] chars = input.Normalize(NormalizationForm.FormD)
            .Where(this.IsNonSpacingMark)
            .ToArray();

        string normalized = new string(chars).Normalize(NormalizationForm.FormC);

        // Return the base implementation.
        return base.ToSlug(normalized);
    }

    private bool IsNonSpacingMark(char c)
    {
        UnicodeCategory category = CharUnicodeInfo.GetUnicodeCategory(c);

        return category != UnicodeCategory.NonSpacingMark;
    }
}
