namespace Nitride.Slugs;

/// <summary>
/// An interface that provides slugs for various paths.
/// </summary>
public interface ISlugConverter
{
    /// <summary>
    /// Converts the given input into a slug.
    /// </summary>
    /// <param name="input">The input string to normalize.</param>
    /// <returns>The resulting slug.</returns>
    string ToSlug(string input);
}
