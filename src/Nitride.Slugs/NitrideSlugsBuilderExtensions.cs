using System;

using Autofac;

namespace Nitride.Slugs;

public static class NitrideSlugsBuilderExtensions
{
    public static NitrideBuilder UseSlugs(
        this NitrideBuilder builder,
        ISlugConverter slugs)
    {
        if (slugs == null)
        {
            throw new ArgumentNullException(nameof(slugs));
        }

        return builder.ConfigureContainer(
            x =>
            {
                x.RegisterInstance(slugs)
                    .As<ISlugConverter>()
                    .SingleInstance();

                x.RegisterModule<NitrideSlugsModule>();
            });
    }
}
