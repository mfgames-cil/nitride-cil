namespace Nitride.Feeds;

/// <summary>
/// A marker component that indicates this page is a feed.
/// </summary>
public class IsFeed
{
    public IsFeed()
    {
    }

    public static IsFeed Instance { get; } = new();
}
