namespace Nitride.Feeds;

/// <summary>
/// A marker component that indicates this entity has a feed associated with
/// it.
/// </summary>
public class HasFeed
{
    public HasFeed()
    {
    }

    public static HasFeed Instance { get; } = new();
}
