using FluentValidation;

namespace Nitride.Feeds;

public class CreateAtomFeedValidator : AbstractValidator<CreateAtomFeed>
{
    public CreateAtomFeedValidator()
    {
        this.RuleFor(x => x.GetEntries)
            .NotNull();

        this.RuleFor(x => x.GetPath)
            .NotNull();

        this.RuleFor(x => x.GetTitle)
            .NotNull();
    }
}
