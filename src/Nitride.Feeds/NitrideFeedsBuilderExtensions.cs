using Autofac;

using Nitride.Temporal;

namespace Nitride.Feeds;

public static class NitrideFeedsBuilderExtensions
{
    public static NitrideBuilder UseFeeds(this NitrideBuilder builder)
    {
        return builder.UseTemporal()
            .ConfigureContainer(x => x.RegisterModule<NitrideFeedsModule>());
    }
}
