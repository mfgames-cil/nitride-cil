using System.Xml.Linq;

namespace Nitride.Feeds.Structure;

/// <summary>
/// Helper methods for working with XML elements.
/// </summary>
public static class AtomHelper
{
    public static void AddIfSet(
        XElement root,
        XElement? elem)
    {
        if (elem != null)
        {
            root.Add(elem);
        }
    }

    public static void AddIfSet(
        XElement elem,
        string name,
        string? text)
    {
        if (!string.IsNullOrWhiteSpace(text))
        {
            elem.Add(new XElement(XmlConstants.AtomNamespace + name, new XText(text)));
        }
    }
}
