using System;
using System.Collections.Generic;
using System.Xml.Linq;

using NodaTime;

using static Nitride.Feeds.Structure.XmlConstants;

namespace Nitride.Feeds.Structure;

/// <summary>
/// The type-safe structure for an entry in the Atom feed.
/// </summary>
[WithProperties]
public partial class AtomEntry
{
    /// <summary>
    /// Gets or sets the author for the feed.
    /// </summary>
    public AtomAuthor? Author { get; set; }

    /// <summary>
    /// Gets or sets the categories associated with this entry.
    /// </summary>
    public IEnumerable<AtomCategory>? Categories { get; set; }

    /// <summary>
    /// Gets or sets the content of the entry.
    /// </summary>
    public string? Content { get; set; }

    /// <summary>
    /// Gets or sets the type of content (text, html) of the content.
    /// </summary>
    public string ContentType { get; set; } = "html";

    /// <summary>
    /// Gets or sets the ID of the feed.
    /// </summary>
    public string? Id { get; set; }

    /// <summary>
    /// Gets or sets the summary of the entry.
    /// </summary>
    public string? Summary { get; set; }

    /// <summary>
    /// Gets or sets the type of content (text, html) of the summary.
    /// </summary>
    public string SummaryType { get; set; } = "html";

    /// <summary>
    /// Gets or sets the title of the Feed.
    /// </summary>
    public string? Title { get; set; }

    /// <summary>
    /// Gets or sets the timestamp that the feed was updated.
    /// </summary>
    public Instant? Updated { get; set; }

    /// <summary>
    /// Gets or sets the URL associated with this feed.
    /// </summary>
    public Uri? Url { get; set; }

    /// <summary>
    /// Creates an XML element out of the feed along with all items inside
    /// the feed.
    /// </summary>
    /// <returns></returns>
    public XElement? ToXElement()
    {
        var elem = new XElement(AtomNamespace + "entry");

        AtomHelper.AddIfSet(elem, "title", this.Title);

        if (this.Url != null)
        {
            elem.Add(
                new XElement(
                    AtomNamespace + "link",
                    new XAttribute("rel", "alternate"),
                    new XAttribute("href", this.Url.ToString())));
        }

        AtomHelper.AddIfSet(elem, "updated", this.Updated?.ToString("g", null));
        AtomHelper.AddIfSet(elem, "id", this.Id);
        AtomHelper.AddIfSet(elem, this.Author?.ToXElement());

        if (this.Categories != null)
        {
            foreach (AtomCategory? category in this.Categories)
            {
                elem.Add(category.ToXElement());
            }
        }

        if (!string.IsNullOrWhiteSpace(this.Summary))
        {
            elem.Add(
                new XElement(
                    AtomNamespace + "summary",
                    new XAttribute("type", this.SummaryType),
                    new XText(this.Summary)));
        }

        if (!string.IsNullOrWhiteSpace(this.Content))
        {
            elem.Add(
                new XElement(
                    AtomNamespace + "content",
                    new XAttribute("type", this.ContentType),
                    new XText(this.Content)));
        }

        return elem;
    }
}
