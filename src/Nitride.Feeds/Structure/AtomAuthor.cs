using System.Xml.Linq;

namespace Nitride.Feeds.Structure;

/// <summary>
/// The type-safe structure for an author element.
/// </summary>
[WithProperties]
public partial class AtomAuthor
{
    /// <summary>
    /// Gets or sets the name of the author.
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Creates an XML element out of the feed along with all items inside
    /// the feed.
    /// </summary>
    /// <returns></returns>
    public XElement? ToXElement()
    {
        if (this.Name == null)
        {
            return null;
        }

        var author = new XElement(XmlConstants.AtomNamespace + "author");

        if (!string.IsNullOrEmpty(this.Name))
        {
            author.Add(new XElement(XmlConstants.AtomNamespace + "name", new XText(this.Name)));
        }

        return author;
    }
}
