using System;
using System.Xml.Linq;

using NodaTime;

using static Nitride.Feeds.Structure.XmlConstants;

namespace Nitride.Feeds.Structure;

/// <summary>
/// The type-safe structure of the top-level feed.
/// </summary>
public record AtomFeed
{
    /// <summary>
    /// Gets or sets the MIME type for the alternate URL.
    /// </summary>
    public string AlternateMimeType { get; set; } = "text/html";

    /// <summary>
    /// Gets or sets the alternate URL for this feed.
    /// </summary>
    public Uri? AlternateUrl { get; set; }

    /// <summary>
    /// Gets or sets the author for the feed.
    /// </summary>
    public AtomAuthor? Author { get; set; }

    /// <summary>
    /// Gets or sets the ID of the feed.
    /// </summary>
    public string? Id { get; set; }

    /// <summary>
    /// Gets or sets the rights (license) of the feed.
    /// </summary>
    public string? Rights { get; set; }

    /// <summary>
    /// Gets or sets the title of the Feed.
    /// </summary>
    public string? Title { get; set; }

    /// <summary>
    /// Gets or sets the timestamp that the feed was updated.
    /// </summary>
    public Instant? Updated { get; set; }

    /// <summary>
    /// Gets or sets the URL associated with this feed.
    /// </summary>
    public Uri? Url { get; set; }

    /// <summary>
    /// Creates an XML element out of the feed along with all items inside
    /// the feed.
    /// </summary>
    /// <returns></returns>
    public XElement ToXElement()
    {
        var elem = new XElement(AtomNamespace + "feed");

        if (!string.IsNullOrWhiteSpace(this.Title))
        {
            elem.Add(
                new XElement(
                    AtomNamespace + "title",
                    new XAttribute("type", "text"),
                    new XAttribute(XNamespace.Xml + "lang", "en"),
                    new XText(this.Title)));
        }

        if (this.Url != null)
        {
            elem.Add(
                new XElement(
                    AtomNamespace + "link",
                    new XAttribute("type", "application/atom+xml"),
                    new XAttribute("href", this.Url.ToString()),
                    new XAttribute("rel", "self")));
        }

        if (this.AlternateUrl != null)
        {
            elem.Add(
                new XElement(
                    AtomNamespace + "link",
                    new XAttribute("type", this.AlternateMimeType),
                    new XAttribute("href", this.AlternateUrl.ToString()),
                    new XAttribute("rel", "alternate")));
        }

        AtomHelper.AddIfSet(elem, "updated", this.Updated?.ToString("g", null));
        AtomHelper.AddIfSet(elem, "id", this.Id);
        AtomHelper.AddIfSet(elem, this.Author?.ToXElement());
        AtomHelper.AddIfSet(elem, "rights", this.Rights);

        return elem;
    }
}
