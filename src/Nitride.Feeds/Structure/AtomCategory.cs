using System;
using System.Xml.Linq;

namespace Nitride.Feeds.Structure;

/// <summary>
/// The type-safe structure for a entry's category element.
/// </summary>
[WithProperties]
public partial class AtomCategory
{
    /// <summary>
    /// Gets or sets the label associated with the category.
    /// </summary>
    public string? Label { get; set; }

    /// <summary>
    /// Gets or sets the scheme associated with the category.
    /// </summary>
    public Uri? Scheme { get; set; }

    /// <summary>
    /// Gets or sets the term of the category.
    /// </summary>
    public string? Term { get; set; }

    /// <summary>
    /// Creates an XML element out of the feed along with all items inside
    /// the feed.
    /// </summary>
    /// <returns></returns>
    public XElement ToXElement()
    {
        if (this.Term == null)
        {
            throw new NullReferenceException("Category term cannot be null.");
        }

        var elem = new XElement(XmlConstants.AtomNamespace + "category", new XAttribute("term", this.Term));

        if (this.Scheme != null)
        {
            elem.Add(new XAttribute("scheme", this.Scheme.ToString()));
        }

        if (!string.IsNullOrEmpty(this.Label))
        {
            elem.Add(new XAttribute("label", this.Label));
        }

        return elem;
    }
}
