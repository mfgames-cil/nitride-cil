using System.Xml.Linq;

namespace Nitride.Feeds.Structure;

/// <summary>
/// Common constants used while generating feeds.
/// </summary>
public static class XmlConstants
{
    /// <summary>
    /// The XML namespace for Atom feeds.
    /// </summary>
    public static readonly XNamespace AtomNamespace = "http://www.w3.org/2005/Atom";

    /// <summary>
    /// The XML namespace for media.
    /// </summary>
    public static readonly XNamespace MediaNamespace = "http://search.yahoo.com/mrss/";
}
