using System;
using System.Collections.Generic;
using System.Linq;

using FluentValidation;

using Gallium;

using Nitride.Contents;
using Nitride.Feeds.Structure;

using NodaTime;

using Serilog;

using Zio;

namespace Nitride.Feeds;

/// <summary>
/// Creates various feeds from the given input.
/// </summary>
[WithProperties]
public partial class CreateAtomFeed : OperationBase
{
    private readonly ILogger logger;

    private readonly IValidator<CreateAtomFeed> validator;

    public CreateAtomFeed(
        ILogger logger,
        IValidator<CreateAtomFeed> validator)
    {
        this.logger = logger;
        this.validator = validator;
        this.GetAlternateMimeType = _ => "text/html";
    }

    /// <summary>
    /// Gets or sets the base URL for all the links.
    /// </summary>
    public string? BaseUrl { get; set; }

    /// <summary>
    /// Gets or sets the alternate MIME type.
    /// </summary>
    public Func<Entity, string> GetAlternateMimeType { get; set; }

    /// <summary>
    /// Gets or sets the alternate URL associated with the feed.
    /// </summary>
    public Func<Entity, Uri>? GetAlternateUrl { get; set; }

    /// <summary>
    /// Gets or sets the callback to get the author for the feed.
    /// </summary>
    public Func<Entity, AtomAuthor>? GetAuthor { get; set; }

    /// <summary>
    /// Gets or sets the callback to get the entries associated with the
    /// feed.
    /// </summary>
    public Func<Entity, IEnumerable<AtomEntry>>? GetEntries { get; set; }

    /// <summary>
    /// Gets or sets the identifier (typically a URL) of the feed.
    /// </summary>
    public Func<Entity, string>? GetId { get; set; }

    /// <summary>
    /// Gets or sets the callback to get the path of the generated feed.
    /// </summary>
    public Func<Entity, UPath>? GetPath { get; set; }

    /// <summary>
    /// Gets or sets the rights (license) of the feed.
    /// </summary>
    public Func<Entity, string>? GetRights { get; set; }

    /// <summary>
    /// A callback that gets the title of the feed from the given entity.
    /// </summary>
    public Func<Entity, string>? GetTitle { get; set; }

    /// <summary>
    /// Gets or sets the updated timestamp for the feed.
    /// </summary>
    public Func<Entity, Instant>? GetUpdated { get; set; }

    /// <summary>
    /// Gets or sets the URL associated with the feed.
    /// </summary>
    public Func<Entity, Uri>? GetUrl { get; set; }

    /// <inheritdoc />
    public override IEnumerable<Entity> Run(IEnumerable<Entity> input)
    {
        this.validator.ValidateAndThrow(this);

        return input.SelectMany(this.CreateEntityFeed);
    }

    private IEnumerable<Entity> CreateEntityFeed(Entity entity)
    {
        // Create the top-level feed. All the nullable callbacks were
        // verified in the function that calls this.
        var feed = new AtomFeed
        {
            Title = this.GetTitle?.Invoke(entity),
            Id = this.GetId?.Invoke(entity),
            Rights = this.GetRights?.Invoke(entity),
            Updated = this.GetUpdated?.Invoke(entity),
            Url = this.GetUrl?.Invoke(entity),
            AlternateUrl = this.GetAlternateUrl?.Invoke(entity),
            AlternateMimeType = this.GetAlternateMimeType.Invoke(entity),
            Author = this.GetAuthor?.Invoke(entity),
        }.ToXElement();

        // Go through all the items inside the feed and add them.
        foreach (AtomEntry? entry in this.GetEntries!(entity))
        {
            feed.Add(entry.ToXElement());
        }

        // Create the feed entity and return both objects.
        Entity feedEntity = new Entity().Set(IsFeed.Instance)
            .Set(this.GetPath!(entity))
            .SetTextContent(feed + "\n");

        return new[] { entity, feedEntity };
    }
}
