using Autofac;

namespace Nitride.Feeds;

public class NitrideFeedsModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
        builder.RegisterValidators(this);
    }
}
