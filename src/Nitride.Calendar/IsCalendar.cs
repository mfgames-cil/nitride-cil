namespace Nitride.Calendar;

/// <summary>
/// A marker component for identifying an entity that represents a calendar.
/// </summary>
public record IsCalendar
{
    public static IsCalendar Instance { get; } = new();
}
