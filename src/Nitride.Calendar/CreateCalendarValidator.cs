using FluentValidation;

namespace Nitride.Calendar;

public class CreateCalendarValidator : AbstractValidator<CreateCalender>
{
    public CreateCalendarValidator()
    {
        this.RuleFor(x => x.Path)
            .NotNull();

        this.RuleFor(x => x.GetEventSummary)
            .NotNull();

        this.RuleFor(x => x.GetEventUrl)
            .NotNull();
    }
}
