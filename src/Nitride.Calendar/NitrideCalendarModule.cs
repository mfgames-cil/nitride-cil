using Autofac;

namespace Nitride.Calendar;

public class NitrideCalendarModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterOperators(this);
        builder.RegisterValidators(this);
    }
}
