using Autofac;

using Nitride.Temporal;

namespace Nitride.Calendar;

public static class NitrideCalendarBuilderExtensions
{
    public static NitrideBuilder UseCalendar(this NitrideBuilder builder)
    {
        return builder.UseTemporal()
            .ConfigureContainer(x => x.RegisterModule<NitrideCalendarModule>());
    }
}
