using Autofac;

namespace Nitride.Yaml;

public class NitrideYamlModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterGeneric(typeof(ParseYamlHeader<>))
            .As(typeof(ParseYamlHeader<>));

        builder.RegisterOperators(this);
        builder.RegisterValidators(this);
    }
}
