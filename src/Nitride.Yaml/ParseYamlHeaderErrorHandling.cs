namespace Nitride.Yaml;

public enum ParseYamlHeaderErrorHandling
{
    /// <summary>
    /// Throw an exception if an error happens.
    /// </summary>
    Throw,

    /// <summary>
    /// If an error happens, then include without additional processing.
    /// </summary>
    Ignore,
}
