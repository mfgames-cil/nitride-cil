namespace Nitride.Yaml;

/// <summary>
/// A marker class that indicates that the entity is YAML.
/// </summary>
public record IsYaml
{
    public static IsYaml Instance { get; } = new();
}
