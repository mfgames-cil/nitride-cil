using System;

using Gallium;

using Nitride.Contents;

using YamlDotNet.Serialization;

namespace Nitride.Yaml;

public static class NitrideYamlEntityExtensions
{
    /// <summary>
    /// Parses the entity text as a YAML file and returns the results.
    /// </summary>
    public static TType? GetYaml<TType>(
        this Entity entity,
        Action<DeserializerBuilder>? configure = null)
    {
        DeserializerBuilder builder = new();

        configure?.Invoke(builder);

        IDeserializer deserializer = builder.Build();

        return entity.GetYaml<TType>(deserializer);
    }

    /// <summary>
    /// Parses the entity text as a YAML file and returns the results.
    /// </summary>
    public static TType? GetYaml<TType>(
        this Entity entity,
        IDeserializer deserializer)
    {
        string? text = entity.GetText();

        return text != null
            ? deserializer.Deserialize<TType>(text)
            : default;
    }
}
