using Autofac;

namespace Nitride.Yaml;

public static class NitrideYamlBuilderExtensions
{
    public static NitrideBuilder UseYaml(this NitrideBuilder builder)
    {
        return builder.ConfigureContainer(x => x.RegisterModule<NitrideYamlModule>());
    }
}
