using Gallium;

namespace Nitride.Yaml;

public static class IsYamlExtensions
{
    public static Entity RemoveIsYaml(this Entity entity)
    {
        return entity.Remove<IsYaml>();
    }

    public static Entity SetIsYaml(this Entity entity)
    {
        return entity.Set(IsYaml.Instance);
    }
}
