namespace Nitride.Yaml;

/// <summary>
/// A marker class that indicates that the has a YAML model.
/// </summary>
public record HasYamlModel
{
    public static HasYamlModel Instance { get; } = new();
}
