using System;
using System.Collections.Generic;
using System.Linq;

using Gallium;

using Nitride.Tests;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Temporal.Tests;

public class CreateDateIndexesTests : TemporalTestBase
    {
        public CreateDateIndexesTests(ITestOutputHelper output)
            : base(output)
        {
        }

    [Fact]
    public void MonthOnlyIndexes()
    {
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();

        CreateDateIndexes op = context.Resolve<CreateDateIndexes>()
            .WithFormats("yyyy-MM")
            .WithCreateIndex(this.CreateIndex);

        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(timekeeper.CreateInstant(2021, 1, 2)),
            new Entity().Add("page2")
                .Add(timekeeper.CreateInstant(2021, 2, 2)),
            new Entity().Add("page3")
                .Add(timekeeper.CreateInstant(2022, 1, 2)),
        };

        List<Tuple<string, List<string>?, List<string>?>> actual = this.GetActual(op, input);

        var expected = new List<Tuple<string, List<string>?, List<string>?>>
        {
            new("index-2021-01", new List<string> { "page1" }, new List<string>()),
            new("index-2021-02", new List<string> { "page2" }, new List<string>()),
            new("index-2022-01", new List<string> { "page3" }, new List<string>()),
            new("page1", null, null),
            new("page2", null, null),
            new("page3", null, null),
        };

        TestHelper.CompareObjects(expected, actual);
    }

    [Fact]
    public void YearMonthDayIndexes()
    {
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();

        CreateDateIndexes op = context.Resolve<CreateDateIndexes>()
            .WithFormats("yyyy/MM/dd", "yyyy/MM", "yyyy")
            .WithCreateIndex(this.CreateIndex);

        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(timekeeper.CreateInstant(2021, 1, 2)),
            new Entity().Add("page2")
                .Add(timekeeper.CreateInstant(2021, 2, 2)),
            new Entity().Add("page3")
                .Add(timekeeper.CreateInstant(2022, 1, 2)),
        };

        List<Tuple<string, List<string>?, List<string>?>> actual = this.GetActual(op, input);

        var expected = new List<Tuple<string, List<string>?, List<string>?>>
        {
            new("index-2021", new List<string>(), new List<string> { "index-2021/01", "index-2021/02" }),
            new("index-2021/01", new List<string>(), new List<string> { "index-2021/01/02" }),
            new("index-2021/01/02", new List<string> { "page1" }, new List<string>()),
            new("index-2021/02", new List<string>(), new List<string> { "index-2021/02/02" }),
            new("index-2021/02/02", new List<string> { "page2" }, new List<string>()),
            new("index-2022", new List<string>(), new List<string> { "index-2022/01" }),
            new("index-2022/01", new List<string>(), new List<string> { "index-2022/01/02" }),
            new("index-2022/01/02", new List<string> { "page3" }, new List<string>()),
            new("page1", null, null),
            new("page2", null, null),
            new("page3", null, null),
        };

        TestHelper.CompareObjects(expected, actual);
    }

    [Fact]
    public void YearMonthDayIndexesThreshold1()
    {
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();

        CreateDateIndexes op = context.Resolve<CreateDateIndexes>()
            .WithFormats("yyyy/MM/dd", "yyyy/MM", "yyyy")
            .WithCreateIndex(this.CreateIndex)
            .WithLessThanEqualCollapse(1);

        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(timekeeper.CreateInstant(2021, 1, 2)),
            new Entity().Add("page2")
                .Add(timekeeper.CreateInstant(2021, 2, 2)),
            new Entity().Add("page3")
                .Add(timekeeper.CreateInstant(2022, 1, 2)),
        };

        List<Tuple<string, List<string>?, List<string>?>> actual = this.GetActual(op, input);

        var expected = new List<Tuple<string, List<string>?, List<string>?>>
        {
            new("index-2021", new List<string>(), new List<string> { "index-2021/01", "index-2021/02" }),
            new("index-2021/01", new List<string> { "page1" }, new List<string> { "index-2021/01/02" }),
            new("index-2021/01/02", new List<string> { "page1" }, new List<string>()),
            new("index-2021/02", new List<string> { "page2" }, new List<string> { "index-2021/02/02" }),
            new("index-2021/02/02", new List<string> { "page2" }, new List<string>()),
            new("index-2022", new List<string> { "page3" }, new List<string> { "index-2022/01" }),
            new("index-2022/01", new List<string> { "page3" }, new List<string> { "index-2022/01/02" }),
            new("index-2022/01/02", new List<string> { "page3" }, new List<string>()),
            new("page1", null, null),
            new("page2", null, null),
            new("page3", null, null),
        };

        TestHelper.CompareObjects(expected, actual);
    }

    [Fact]
    public void YearMonthIndexes()
    {
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();

        CreateDateIndexes op = context.Resolve<CreateDateIndexes>()
            .WithFormats("yyyy-MM", "yyyy")
            .WithCreateIndex(this.CreateIndex);

        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(timekeeper.CreateInstant(2021, 1, 2)),
            new Entity().Add("page2")
                .Add(timekeeper.CreateInstant(2021, 2, 2)),
            new Entity().Add("page3")
                .Add(timekeeper.CreateInstant(2022, 1, 2)),
        };

        List<Tuple<string, List<string>?, List<string>?>> actual = this.GetActual(op, input);

        var expected = new List<Tuple<string, List<string>?, List<string>?>>
        {
            new("index-2021", new List<string>(), new List<string> { "index-2021-01", "index-2021-02" }),
            new("index-2021-01", new List<string> { "page1" }, new List<string>()),
            new("index-2021-02", new List<string> { "page2" }, new List<string>()),
            new("index-2022", new List<string>(), new List<string> { "index-2022-01" }),
            new("index-2022-01", new List<string> { "page3" }, new List<string>()),
            new("page1", null, null),
            new("page2", null, null),
            new("page3", null, null),
        };

        TestHelper.CompareObjects(expected, actual);
    }

    [Fact]
    public void YearOnlyIndexes()
    {
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();

        CreateDateIndexes op = context.Resolve<CreateDateIndexes>()
            .WithFormats("yyyy")
            .WithCreateIndex(this.CreateIndex);

        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(timekeeper.CreateInstant(2021, 1, 2)),
            new Entity().Add("page2")
                .Add(timekeeper.CreateInstant(2021, 2, 2)),
            new Entity().Add("page3")
                .Add(timekeeper.CreateInstant(2022, 1, 2)),
        };

        List<Tuple<string, List<string>?, List<string>?>> actual = this.GetActual(op, input);

        var expected = new List<Tuple<string, List<string>?, List<string>?>>
        {
            new("index-2021", new List<string> { "page1", "page2" }, new List<string>()),
            new("index-2022", new List<string> { "page3" }, new List<string>()),
            new("page1", null, null),
            new("page2", null, null),
            new("page3", null, null),
        };

        TestHelper.CompareObjects(expected, actual);
    }

    private Entity CreateIndex(DateIndex a)
    {
        return new Entity().Add(a)
            .Add($"index-{a.Key}");
    }

    private List<Tuple<string, List<string>?, List<string>?>> GetActual(
        CreateDateIndexes op,
        List<Entity> input)
    {
        var actual = op.Run(input)
            .Select(
                x => new Tuple<string, List<string>?, List<string>?>(
                    x.Get<string>(),
                    x.GetOptional<DateIndex>()
                        ?.Entries.Select(a => a.Get<string>())
                        .OrderBy(x => x)
                        .ToList(),
                    x.GetOptional<DateIndex>()
                        ?.Indexes.Select(a => a.Get<string>())
                        .OrderBy(x => x)
                        .ToList()))
            .OrderBy(x => x.Item1)
            .ToList();

        return actual;
    }
}
