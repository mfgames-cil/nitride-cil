using System.Collections.Generic;

using Gallium;

using NodaTime;
using NodaTime.Testing;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Temporal.Tests;

public class FilterOutFutureInstantTests : TemporalTestBase
{
    public FilterOutFutureInstantTests(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void FiltersOutFutureInstants()
    {
        // Create the context and set the timestamp to a constant value.
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();
        var now = Instant.FromUtc(2000, 6, 1, 0, 0);

        timekeeper.Clock = new FakeClock(now);

        // Create the entities.
        List<Entity> input = new()
        {
            new Entity()
                .Add("past")
                .Add(Instant.FromUtc(1990, 6, 1, 0, 0)),
            new Entity()
                .Add("future")
                .Add(Instant.FromUtc(2020, 6, 1, 0, 0)),
        };

        // Create the operation and run it.
        FilterOutFutureInstant op = context.Resolve<FilterOutFutureInstant>();
        IEnumerable<Entity> output = input.Run(op);

        // Verify the values.
        Entity entity = Assert.Single(output);
        Assert.Equal("past", entity.Get<string>());
    }

    [Fact]
    public void KeepsNonInstant()
    {
        // Create the context and set the timestamp to a constant value.
        using TemporalTestContext context = this.CreateContext();
        Timekeeper timekeeper = context.Resolve<Timekeeper>();
        var now = Instant.FromUtc(2000, 6, 1, 0, 0);

        timekeeper.Clock = new FakeClock(now);

        // Create the entities.
        List<Entity> input = new()
        {
            new Entity()
                .Add("neither"),
            new Entity()
                .Add("future")
                .Add(Instant.FromUtc(2020, 6, 1, 0, 0)),
        };

        // Create the operation and run it.
        FilterOutFutureInstant op = context.Resolve<FilterOutFutureInstant>();
        IEnumerable<Entity> output = input.Run(op);

        // Verify the values.
        Entity entity = Assert.Single(output);
        Assert.Equal("neither", entity.Get<string>());
    }
}
