using Autofac;

using Nitride.Tests;

namespace Nitride.Temporal.Tests;

public class TemporalTestContext : NitrideTestContext
{
    /// <inheritdoc />
    protected override void ConfigureContainer(ContainerBuilder builder)
    {
        base.ConfigureContainer(builder);
        builder.RegisterModule<NitrideTemporalModule>();
    }
}
