using MfGames.TestSetup;

using Xunit.Abstractions;

namespace Nitride.Temporal.Tests;

public abstract class TemporalTestBase : TestBase<TemporalTestContext>
{
    protected TemporalTestBase(ITestOutputHelper output)
        : base(output)
    {
    }
}
