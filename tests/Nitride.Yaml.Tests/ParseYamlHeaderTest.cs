using System.Collections.Generic;
using System.Linq;

using Gallium;

using Nitride.Contents;
using Nitride.Tests;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Yaml.Tests;

public class ParseYamlHeaderTest : NitrideTestBase
{
    public ParseYamlHeaderTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void ParseNoContent()
    {
        var input = new List<Entity>
        {
            new Entity().SetTextContent(new StringTextContent("---", "title: Test Title 1", "---"))
        };

        var op = new ParseYamlHeader<TestHeader>();

        Entity output = input.Run(op)
            .First();

        Assert.True(output.Has<ITextContent>());

        Assert.Equal(
            new[]
            {
                string.Empty,
            },
            output.Get<ITextContent>()
                .GetText()
                .Split("\n"));

        Assert.True(output.Has<TestHeader>());

        Assert.Equal(
            "Test Title 1",
            output.Get<TestHeader>()
                .Title);
    }

    [Fact]
    public void ParseNoHeader()
    {
        var input = new List<Entity>
        {
            new Entity().SetTextContent(new StringTextContent("Hello"))
        };

        var op = new ParseYamlHeader<TestHeader>();

        Entity output = input.Run(op)
            .First();

        Assert.True(output.Has<ITextContent>());

        Assert.Equal(
            new[]
            {
                "Hello",
                string.Empty,
            },
            output.Get<ITextContent>()
                .GetText()
                .Split("\n"));

        Assert.True(output.Has<TestHeader>());

        Assert.Null(
            output.Get<TestHeader>()
                .Title);
    }

    [Fact]
    public void ParseSimpleHeader()
    {
        var input = new List<Entity>
        {
            new Entity().SetTextContent(new StringTextContent("---", "title: Test Title 1", "---", "Hello"))
        };

        var op = new ParseYamlHeader<TestHeader>();

        Entity output = input.Run(op)
            .First();

        Assert.True(output.Has<ITextContent>());

        Assert.Equal(
            new[]
            {
                "Hello",
                string.Empty,
            },
            output.Get<ITextContent>()
                .GetText()
                .Split("\n"));

        Assert.True(output.Has<TestHeader>());

        Assert.Equal(
            "Test Title 1",
            output.Get<TestHeader>()
                .Title);
    }

    private class TestHeader
    {
        public string? Title { get; set; }
    }
}
