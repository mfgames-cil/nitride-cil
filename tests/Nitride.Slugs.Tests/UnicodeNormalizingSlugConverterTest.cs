using System.Collections.Generic;

using Nitride.Tests;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Slugs.Tests;

/// <summary>
/// Tests the functionality of the WriteFiles().
/// </summary>
public class UnicodeNormalizingSlugConverterTest : NitrideTestBase
{
    public UnicodeNormalizingSlugConverterTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void DefaultConversions()
    {
        var slug = new UnicodeNormalizingSlugConverter();
        var expected = new List<string>();
        var actual = new List<string>();

        this.Test(slug, expected, actual, "One", "one");
        this.Test(slug, expected, actual, "Two-Words", "two-words");
        this.Test(slug, expected, actual, "Rutejìmo", "rutejimo");
        this.Test(slug, expected, actual, "C#", "c");

        Assert.Equal(expected, actual);
    }

    [Fact]
    public void ReplacementConversions()
    {
        var slug = new UnicodeNormalizingSlugConverter(
            new Dictionary<string, string>
            {
                ["#"] = "-sharp",
            });

        var expected = new List<string>();
        var actual = new List<string>();

        this.Test(slug, expected, actual, "One", "one");
        this.Test(slug, expected, actual, "Two-Words", "two-words");
        this.Test(slug, expected, actual, "Rutejìmo", "rutejimo");
        this.Test(slug, expected, actual, "C#", "c-sharp");

        Assert.Equal(expected, actual);
    }

    private void Test(
        ISlugConverter slug,
        List<string> expected,
        List<string> actual,
        string input,
        string output)
    {
        expected.Add(output);
        actual.Add(slug.ToSlug(input));
    }
}
