using Autofac;

using MfGames.TestSetup;

namespace Nitride.Tests;

/// <summary>
/// The base test context for Nitride tests.
/// </summary>
public class NitrideTestContext : TestContext
{
    /// <inheritdoc />
    protected override void ConfigureContainer(ContainerBuilder builder)
    {
        base.ConfigureContainer(builder);
        builder.RegisterModule<NitrideModule>();
    }
}
