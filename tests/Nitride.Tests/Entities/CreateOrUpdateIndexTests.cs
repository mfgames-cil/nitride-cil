using System;
using System.Collections.Generic;
using System.Linq;

using Gallium;

using Nitride.Entities;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Tests.Entities;

public class CreateOrUpdateIndexTests : NitrideTestBase, IDisposable
{
    private readonly NitrideTestContext context;

    private readonly IOperation op;

    private readonly EntityScanner scanner;

    public CreateOrUpdateIndexTests(ITestOutputHelper output)
        : base(output)
    {
        this.context = this.CreateContext();

        this.scanner = this.context.Resolve<EntityScanner>()
            .WithGetKeysFromEntity(e => e.GetOptional<List<string>>());

        this.op = this.context.Resolve<CreateOrUpdateIndex>()
            .WithScanner(this.scanner)
            .WithGetIndexKey(
                x => x.Get<string>()
                    .Contains("index")
                    ? x.Get<string>()
                        .Replace("index", "cat")
                    : null)
            .WithCreateIndex(
                (
                    key,
                    list) => new Entity().Add(key.Replace("cat", "index"))
                    .Add(list.ToList()))
            .WithUpdateIndex(
                (
                    index,
                    key,
                    list) => index.Add(list.ToList()));
    }

    [Fact]
    public void CreateMultipleIndexes()
    {
        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(new List<string> { "cat1" }),
            new Entity().Add("page2")
                .Add(new List<string> { "cat2" }),
            new Entity().Add("page3")
                .Add(new List<string> { "cat1" }),
        };

        List<Tuple<string, List<string>?>> actual = this.GetActual(input);

        Assert.Equal(
            new[]
            {
                new Tuple<string, List<string>?>("index1", new List<string> { "page1", "page3" }),
                new Tuple<string, List<string>?>("index2", new List<string> { "page2" }),
                new Tuple<string, List<string>?>("page1", null),
                new Tuple<string, List<string>?>("page2", null),
                new Tuple<string, List<string>?>("page3", null),
            },
            actual);
    }

    [Fact]
    public void CreateNestedIndexes()
    {
        List<Entity> input = new()
        {
            new Entity().Add("index2")
                .Add(new List<string> { "cat1" }),
            new Entity().Add("page2")
                .Add(new List<string> { "cat2" }),
            new Entity().Add("page3")
                .Add(new List<string> { "cat1" }),
        };

        List<Tuple<string, List<string>?>> actual = this.GetActual(input);

        Assert.Equal(
            new[]
            {
                new Tuple<string, List<string>?>("index1", new List<string> { "index2", "page3" }),
                new Tuple<string, List<string>?>("index2", new List<string> { "page2" }),
                new Tuple<string, List<string>?>("page2", null),
                new Tuple<string, List<string>?>("page3", null),
            },
            actual);
    }

    [Fact]
    public void CreateSimpleIndex()
    {
        List<Entity> input = new()
        {
            new Entity().Add("page1")
                .Add(new List<string> { "cat1" }),
        };

        List<Tuple<string, List<string>?>> actual = this.GetActual(input);

        Assert.Equal(
            new[]
            {
                new Tuple<string, List<string>?>("index1", new List<string> { "page1" }),
                new Tuple<string, List<string>?>("page1", null),
            },
            actual);
    }

    /// <inheritdoc />
    public void Dispose()
    {
        this.context.Dispose();
    }

    [Fact]
    public void UpdateSimpleIndex()
    {
        List<Entity> input = new()
        {
            new Entity().Add("index1"),
            new Entity().Add("page1")
                .Add(new List<string> { "cat1" }),
        };

        var output = this.scanner.Run(input)
            .ToList()
            .Run(this.op)
            .ToList();

        var actual = output
            .Select(
                x => new Tuple<string, List<string>?>(
                    x.Get<string>(),
                    x.GetOptional<List<Entity>>()
                        ?.Select(y => y.Get<string>())
                        .OrderBy(y => y)
                        .ToList()))
            .OrderBy(x => x.Item1)
            .ToList();

        Assert.Equal(
            new[]
            {
                new Tuple<string, List<string>?>("index1", new List<string> { "page1" }),
                new Tuple<string, List<string>?>("page1", null),
            },
            actual);
    }

    private List<Tuple<string, List<string>?>> GetActual(List<Entity> input)
    {
        var output = this.scanner.Run(input)
            .ToList()
            .Run(this.op)
            .ToList();

        var actual = output
            .Select(
                x => new Tuple<string, List<string>?>(
                    x.Get<string>(),
                    x.GetOptional<List<Entity>>()
                        ?.Select(y => y.Get<string>())
                        .OrderBy(y => y)
                        .ToList()))
            .OrderBy(x => x.Item1)
            .ToList();

        return actual;
    }
}
