using System.Collections.Generic;
using System.Linq;

using Gallium;

using Nitride.Entities;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Tests.Entities;

public class LinkEntitySequenceTests : NitrideTestBase
{
    public LinkEntitySequenceTests(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void LinkThree()
    {
        NitrideTestContext context = this.CreateContext();
        LinkEntitySequence op = context.Resolve<LinkEntitySequence>();

        var input = new List<Entity>
        {
            new Entity().Add("page1"),
            new Entity().Add("page2"),
            new Entity().Add("page3"),
        };

        var results = op.Run(input)
            .ToList();

        Assert.Equal(3, results.Count);

        EntitySequence seq1 = results[0]
            .Get<EntitySequence>();

        EntitySequence seq2 = results[1]
            .Get<EntitySequence>();

        EntitySequence seq3 = results[2]
            .Get<EntitySequence>();

        Assert.True(seq1.IsFirst);
        Assert.False(seq1.IsLast);
        Assert.False(seq1.HasPrevious);
        Assert.True(seq1.HasNext);
        Assert.Null(seq1.Previous?.Get<string>());
        Assert.Equal("page2", seq1.Next!.Get<string>());

        Assert.False(seq2.IsFirst);
        Assert.False(seq2.IsLast);
        Assert.True(seq2.HasPrevious);
        Assert.True(seq2.HasNext);
        Assert.Equal("page1", seq2.Previous!.Get<string>());
        Assert.Equal("page3", seq2.Next!.Get<string>());

        Assert.False(seq3.IsFirst);
        Assert.True(seq3.IsLast);
        Assert.True(seq3.HasPrevious);
        Assert.False(seq3.HasNext);
        Assert.Equal("page2", seq3.Previous!.Get<string>());
        Assert.Null(seq3.Next?.Get<string>());
    }
}
