using System.Text;

using KellermanSoftware.CompareNetObjects;

using Newtonsoft.Json;

using Xunit.Sdk;

namespace Nitride.Tests;

public static class TestHelper
{
    public static void CompareObjects<T>(
        T expected,
        T actual)
        where T : class
    {
        CompareLogic compare = new()
        {
            Config =
            {
                MaxDifferences = int.MaxValue,
            },
        };

        ComparisonResult comparison = compare.Compare(expected, actual);

        if (comparison.AreEqual)
        {
            return;
        }

        // Format the error message.
        StringBuilder message = new();

        message.AppendLine("# Expected");
        message.AppendLine();
        message.AppendLine(JsonConvert.SerializeObject(expected, Formatting.Indented));
        message.AppendLine();
        message.AppendLine("# Actual");
        message.AppendLine();
        message.AppendLine(JsonConvert.SerializeObject(actual, Formatting.Indented));
        message.AppendLine();
        message.Append("# Results");
        message.AppendLine();
        message.AppendLine(comparison.DifferencesString);

        throw new XunitException(message.ToString());
    }
}
