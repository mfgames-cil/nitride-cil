using MfGames.TestSetup;

using Xunit.Abstractions;

namespace Nitride.Tests;

/// <summary>
/// Common initialization logic for Nitride-based tests including setting
/// up containers, logging, and Serilog.
/// </summary>
public abstract class NitrideTestBase : TestBase<NitrideTestContext>
{
    protected NitrideTestBase(ITestOutputHelper output)
        : base(output)
    {
    }
}
