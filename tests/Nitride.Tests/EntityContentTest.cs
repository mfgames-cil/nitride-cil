using Gallium;

using Nitride.Contents;

using Xunit;
using Xunit.Abstractions;

namespace Nitride.Tests;

/// <summary>
/// Tests the various functionality of the high-level content methods and
/// extension methods.
/// </summary>
public class EntityContentTest : NitrideTestBase
{
    public EntityContentTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void AddBinaryContent()
    {
        var content = new ByteArrayBinaryContent(new byte[0]);
        Entity entity = new Entity().SetBinaryContent(content);

        Assert.Equal(1, entity.Count);
        Assert.True(entity.HasContent());
        Assert.True(entity.HasBinaryContent());
        Assert.False(entity.HasTextContent());
    }

    [Fact]
    public void AddTextContent()
    {
        var content = new StringTextContent("contents");
        Entity entity = new Entity().SetTextContent(content);

        Assert.Equal(1, entity.Count);
        Assert.True(entity.HasContent());
        Assert.False(entity.HasBinaryContent());
        Assert.True(entity.HasTextContent());
    }

    [Fact]
    public void SwitchBinaryToTextContent()
    {
        var binaryContent = new ByteArrayBinaryContent(new byte[0]);
        var textContent = new StringTextContent("contents");

        Entity entity = new Entity().SetBinaryContent(binaryContent)
            .SetTextContent(textContent);

        Assert.Equal(1, entity.Count);
        Assert.True(entity.HasContent());
        Assert.False(entity.HasBinaryContent());
        Assert.True(entity.HasTextContent());
    }

    [Fact]
    public void SwitchTextToBinaryContent()
    {
        var binaryContent = new ByteArrayBinaryContent(new byte[0]);
        var textContent = new StringTextContent("contents");

        Entity entity = new Entity().SetContent<ITextContent>(textContent)
            .SetContent<IBinaryContent>(binaryContent);

        Assert.Equal(1, entity.Count);
        Assert.True(entity.HasContent());
        Assert.True(entity.HasBinaryContent());
        Assert.False(entity.HasTextContent());
    }
}
