using System.Linq;

using Nitride.Contents;
using Nitride.IO.Contents;

using Xunit;
using Xunit.Abstractions;

using Zio;
using Zio.FileSystems;

namespace Nitride.IO.Tests;

/// <summary>
/// Tests the functionality of the WriteFiles().
/// </summary>
public class WriteFilesTest : NitrideIOTestBase
{
    public WriteFilesTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void WriteAllFiles()
    {
        // Set up the test.
        using NitrideIOTestContext context = this.CreateContext();

        // Set up the file.
        IFileSystem fileSystem = context.FileSystem;

        fileSystem.CreateDirectory("/b1");
        fileSystem.CreateDirectory("/c1");
        fileSystem.CreateDirectory("/c1/c2");
        fileSystem.CreateDirectory("/d1");
        fileSystem.WriteAllText("/a.txt", "File A");
        fileSystem.WriteAllText("/b1/b.md", "File B");
        fileSystem.WriteAllText("/c1/c2/e.md", "File E");

        // Set up the operation.
        var output = new MemoryFileSystem();
        ReadFiles readFiles = context.Resolve<ReadFiles>();

        WriteFiles op = context.Resolve<WriteFiles>()
            .WithFileSystem(output);

        // Read and write out the files. We switch one of the files to be
        // text content to make sure that works too.
        readFiles.WithPattern("/**")
            .Run()
            .Select(
                x => x.Get<UPath>() == "/b1/b.md"
                    ? x.SetTextContent(((ITextContentConvertable)x.GetBinaryContent()).ToTextContent())
                    : x)
            .Run(op);

        // Verify the results.
        Assert.True(output.FileExists("/a.txt"));
        Assert.True(output.FileExists("/b1/b.md"));
        Assert.True(output.FileExists("/c1/c2/e.md"));

        Assert.Equal("File A", output.ReadAllText("/a.txt"));
        Assert.Equal("File B", output.ReadAllText("/b1/b.md"));
        Assert.Equal("File E", output.ReadAllText("/c1/c2/e.md"));
    }
}
