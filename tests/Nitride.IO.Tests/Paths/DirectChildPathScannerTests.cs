using System.Collections.Generic;
using System.Linq;

using Nitride.IO.Contents;
using Nitride.IO.Paths;
using Nitride.Tests;

using Xunit;
using Xunit.Abstractions;

using Zio;

namespace Nitride.IO.Tests;

public class DirectChildPathScannerTests : NitrideIOTestBase
{
    public DirectChildPathScannerTests(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void NestedFoldersTests()
    {
        // Set up the test.
        using NitrideIOTestContext context = this.CreateContext();

        // Set up the file.
        IFileSystem fileSystem = context.FileSystem;

        fileSystem.CreateDirectory("/a");
        fileSystem.CreateDirectory("/b");
        fileSystem.CreateDirectory("/a/c");
        fileSystem.CreateDirectory("/a/d");
        fileSystem.CreateDirectory("/a/d/e");
        fileSystem.CreateFile("/index.md");
        fileSystem.CreateFile("/a/index.md");
        fileSystem.CreateFile("/b/index.md");
        fileSystem.CreateFile("/a/c/index.md");
        fileSystem.CreateFile("/a/d/index.md");
        fileSystem.CreateFile("/a/d/e/index.md");

        // Set up the operation.
        ReadFiles readFiles = context.Resolve<ReadFiles>();
        DirectChildPathScanner op = context.Resolve<DirectChildPathScanner>();

        // Read and replace the paths.
        var _ = readFiles.WithPattern("/**")
            .Run()
            .Run(op)
            .ToList();

        KeyValuePair<string, string[]>[] actual = op.GetScannedResults()
            .ToDictionary(
                x => x.Key,
                x => x.Value.Select(
                        y => y.Get<UPath>()
                            .ToString())
                    .ToArray())
            .ToList()
            .OrderBy(x => x.Key)
            .ToArray();

        // Verify the results.
        KeyValuePair<string, string[]>[] expected = new[]
        {
            new KeyValuePair<string, string[]>("/", new[] { "/a/index.md", "/b/index.md" }),
            new KeyValuePair<string, string[]>("/a/", new[] { "/a/c/index.md", "/a/d/index.md" }),
            new KeyValuePair<string, string[]>("/a/d/", new[] { "/a/d/e/index.md" }),
        };

        TestHelper.CompareObjects(expected, actual);
    }
}
