using System.Linq;

using MAB.DotIgnore;

using Nitride.IO.Contents;

using Xunit;
using Xunit.Abstractions;

using Zio;

namespace Nitride.IO.Tests;

public class WhereNotIgnoredTests : NitrideIOTestBase
{
    public WhereNotIgnoredTests(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void DoesIgnore()
    {
        // Set up the test.
        using NitrideIOTestContext context = this.CreateContext();

        // Set up the file.
        IFileSystem fileSystem = context.FileSystem;

        fileSystem.CreateFile("/b1.txt");
        fileSystem.CreateFile("/c1.md");
        fileSystem.CreateDirectory("/d");
        fileSystem.CreateFile("/d/b2.txt");

        // Set up the ignore file.
        var ignore = new IgnoreList(new[] { "*.txt" });

        // Set up the operation.
        ReadFiles readFiles = context.Resolve<ReadFiles>();

        // Read and replace the paths.
        IOrderedEnumerable<string> output = readFiles.WithPattern("/**")
            .Run()
            .WhereNotIgnored(ignore)
            .Select(
                x => x.Get<UPath>()
                    .ToString())
            .OrderBy(x => x);

        // Verify the results.
        Assert.Equal(
            new[]
            {
                "/c1.md",
            },
            output);
    }
}
