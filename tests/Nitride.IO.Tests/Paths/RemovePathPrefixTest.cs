using System.Linq;

using Nitride.IO.Contents;
using Nitride.IO.Paths;

using Xunit;
using Xunit.Abstractions;

using Zio;

namespace Nitride.IO.Tests;

public class RemovePathPrefixTest : NitrideIOTestBase
{
    public RemovePathPrefixTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void PrefixAllFiles()
    {
        // Set up the test.
        using NitrideIOTestContext context = this.CreateContext();

        // Set up the file.
        IFileSystem fileSystem = context.FileSystem;

        fileSystem.CreateDirectory("/a");
        fileSystem.CreateDirectory("/a/a");
        fileSystem.CreateFile("/a/b1.txt");
        fileSystem.CreateFile("/a/a/c1.md");

        // Set up the operation.
        ReadFiles readFiles = context.Resolve<ReadFiles>();

        RemovePathPrefix op = context.Resolve<RemovePathPrefix>()
            .WithPathPrefix("/a");

        // Read and replace the paths.
        IOrderedEnumerable<string> output = readFiles.WithPattern("/**")
            .Run()
            .Run(op)
            .Select(
                x => x.Get<UPath>()
                    .ToString())
            .OrderBy(x => x);

        // Verify the results.
        Assert.Equal(
            new[]
            {
                "/a/c1.md",
                "/b1.txt",
            },
            output);
    }
}
