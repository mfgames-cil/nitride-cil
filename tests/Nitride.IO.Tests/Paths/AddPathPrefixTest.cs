using System.Linq;

using Nitride.IO.Contents;
using Nitride.IO.Paths;

using Xunit;
using Xunit.Abstractions;

using Zio;

namespace Nitride.IO.Tests;

public class AddPathPrefixTest : NitrideIOTestBase
{
    public AddPathPrefixTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void PrefixAllFiles()
    {
        // Set up the test.
        using NitrideIOTestContext context = this.CreateContext();

        // Set up the file.
        IFileSystem fileSystem = context.FileSystem;

        fileSystem.CreateFile("/b1.txt");
        fileSystem.CreateFile("/c1.md");

        // Set up the operation.
        ReadFiles readFiles = context.Resolve<ReadFiles>();

        AddPathPrefix op = context.Resolve<AddPathPrefix>()
            .WithPathPrefix("/prefix");

        // Read and replace the paths.
        IOrderedEnumerable<string> output = readFiles.WithPattern("/**")
            .Run()
            .Run(op)
            .Select(
                x => x.Get<UPath>()
                    .ToString())
            .OrderBy(x => x);

        // Verify the results.
        Assert.Equal(
            new[]
            {
                "/prefix/b1.txt",
                "/prefix/c1.md",
            },
            output);
    }
}
