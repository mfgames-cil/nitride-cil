using System.Linq;

using Nitride.IO.Contents;
using Nitride.IO.Paths;

using Xunit;
using Xunit.Abstractions;

using Zio;

namespace Nitride.IO.Tests;

public class MoveToIndexPathsTest : NitrideIOTestBase
{
    public MoveToIndexPathsTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public void MoveAllFiles()
    {
        // Set up the test and file system.
        using NitrideIOTestContext context = this.CreateContext();

        CreateFileSystem(context);

        // Set up the operation.
        ReadFiles readFiles = context.Resolve<ReadFiles>();
        MoveToIndexPath op = context.Resolve<MoveToIndexPath>();

        // Read and replace the paths.
        IOrderedEnumerable<string> output = readFiles.WithPattern("/**")
            .Run()
            .Run(op)
            .Select(
                x => x.Get<UPath>()
                    .ToString())
            .OrderBy(x => x);

        // Verify the results.
        Assert.Equal(
            new[]
            {
                "/a1",
                "/b1.txt",
                "/c1/index.md",
                "/d1/index.html",
            },
            output);
    }

    [Fact]
    public void OverrideCanMoveCallback()
    {
        // Set up the test and file system.
        using NitrideIOTestContext context = this.CreateContext();

        CreateFileSystem(context);

        // Set up the operation.
        ReadFiles readFiles = context.Resolve<ReadFiles>();

        MoveToIndexPath op = context.Resolve<MoveToIndexPath>()
            .WithCanMoveCallback(
                path => path.ToString()
                    .Contains("a1"));

        // Read and replace the paths.
        IOrderedEnumerable<string> output = readFiles.WithPattern("/**")
            .Run()
            .Run(op)
            .Select(
                x => x.Get<UPath>()
                    .ToString())
            .OrderBy(x => x);

        // Verify the results.
        Assert.Equal(
            new[]
            {
                "/a1/index",
                "/b1.txt",
                "/c1/index.md",
                "/d1.html",
            },
            output);
    }

    private static void CreateFileSystem(NitrideIOTestContext context)
    {
        // Set up the file system.
        IFileSystem fileSystem = context.FileSystem;

        fileSystem.CreateDirectory("/c1");
        fileSystem.CreateFile("/a1");
        fileSystem.CreateFile("/b1.txt");
        fileSystem.CreateFile("/c1/index.md");
        fileSystem.CreateFile("/d1.html");
    }
}
