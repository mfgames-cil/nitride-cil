using Autofac;

using Nitride.Tests;

using Zio;
using Zio.FileSystems;

namespace Nitride.IO.Tests;

public class NitrideIOTestContext : NitrideTestContext
{
    public IFileSystem FileSystem => this.Resolve<IFileSystem>();

    /// <inheritdoc />
    protected override void ConfigureContainer(ContainerBuilder builder)
    {
        base.ConfigureContainer(builder);
        builder.RegisterModule<NitrideIOModule>();

        builder.RegisterInstance(new MemoryFileSystem())
            .As<IFileSystem>()
            .SingleInstance();
    }
}
