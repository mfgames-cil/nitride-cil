using MfGames.TestSetup;

using Xunit.Abstractions;

namespace Nitride.IO.Tests;

public abstract class NitrideIOTestBase : TestBase<NitrideIOTestContext>
{
    protected NitrideIOTestBase(ITestOutputHelper output)
        : base(output)
    {
    }
}
