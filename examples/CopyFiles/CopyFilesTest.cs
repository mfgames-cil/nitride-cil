using System.IO;
using System.Threading.Tasks;

using CliWrap;

using MfGames.IO.Extensions;

using Nitride.Tests;

using Xunit;
using Xunit.Abstractions;

namespace CopyFiles;

/// <summary>
/// Tests the execution of the tool and ensures it is working correctly.
/// </summary>
public class CopyFilesTest : NitrideTestBase
{
    public CopyFilesTest(ITestOutputHelper output)
        : base(output)
    {
    }

    [Fact]
    public async Task Run()
    {
        // Figure out the paths for this test.
        DirectoryInfo rootDir =
            typeof(CopyFilesProgram).GetDirectory()!.FindGitRoot()!.GetDirectory("examples/CopyFiles");

        DirectoryInfo outputDir = rootDir.GetDirectory("output");
        FileInfo projectFile = rootDir.GetFile("CopyFiles.csproj");

        this.Logger.Error("A {0}", rootDir);

        // Clear out the output directory if we have an old one.
        if (outputDir.Exists)
        {
            outputDir.Delete(true);
        }

        // Execute the generator. This will throw if there is an exception.
        await Cli.Wrap("dotnet")
            .WithArguments(
                x => x.Add("run")
                    .Add("--project")
                    .Add(projectFile.FullName)
                    .Add("--")
                    .Add("build"))
            .ExecuteAsync();

        // Make sure we have our output.
        FileInfo aFile = outputDir.GetFile("a.txt");

        Assert.True(aFile.Exists);

        string aText = aFile.ReadAllText()
            .Trim();

        Assert.Equal("This is the 'A' file.", aText);
    }
}
