using System.IO;
using System.Threading.Tasks;

using Autofac;

using MfGames.IO.Extensions;

using Nitride;
using Nitride.IO;

namespace CopyFiles;

/// <summary>
/// Main entry point into the CopyFiles sample generator.
/// </summary>
public static class CopyFilesProgram
{
    public static async Task<int> Main(string[] args)
    {
        // All of the builder methods are fluent in that they return the
        // builder to allow them to be chained. However, for documentation
        // purposes, we are going to split them apart to explain the details.
        var builder = new NitrideBuilder(args);

        // Filesystem access is provided by Zio, which allows for mutliple
        // folders to be combined together into a single unified file
        // system. At the moment, we set the "root" directory which will
        // contains all the paths, both input and output.
        DirectoryInfo rootDir =
            typeof(CopyFilesProgram).GetDirectory()!.FindGitRoot()!.GetDirectory("examples/CopyFiles");

        builder.WithRootDirectory(rootDir);

        // Like Serilog, we use a number of extension methods on the builder
        // to inject functionality such as plugins and extensions. In this
        // case, we only need the Nitride.IO module so we use the `UseIO`
        // to inject the requisite modules and configure it.
        builder.UseIO();

        // We use Autofac for the bulk of our registration handling. This
        // was mainly because we are more comfortable with Autofac, but it
        // also has a clean interface for handling some of the more esoteric
        // problems we've encountered.
        builder.ConfigureContainer(x => x.RegisterModule<CopyFilesModule>());

        // Finally, we build the site generator object and run it.
        return await builder.RunAsync();
    }
}
