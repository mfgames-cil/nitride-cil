# Copy Files

This is probably the most basic generator possible. It simply copies files from
the input and places them into the output. However, it also demonstrates a basic
setup including creating a pipeline, wiring everything up with modules, and
configuring everything.
