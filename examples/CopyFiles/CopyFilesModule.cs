using Autofac;

namespace CopyFiles;

public class CopyFilesModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        // This just registers all the non-static classes as singletons
        // within the system. We use lifetimes in other components depending
        // on how they are used, but in this case, we don't need it.
        builder.RegisterAssemblyTypes(
                this.GetType()
                    .Assembly)
            .AsSelf()
            .AsImplementedInterfaces()
            .SingleInstance();
    }
}
